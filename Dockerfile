FROM rasa/rasa:1.10.8

COPY . /app

CMD [ "run", " -m"," models"," --enable-api"," --log-file "," rasa_log.log"," --endpoints"," endpoints-docker.yml"," --port ","5005"," --verbose"," --debug"]
