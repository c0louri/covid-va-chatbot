# aggregate_gen.py
# 
# input: domain.yml (reads it by default)
# output: 'body' of a mongo addField state.
#
# Creates the body of the mongo aggregate command
# that adds the fields times_covid_stats, time_symptoms, times_icu_stats e.t.c.
# (i.e the counts of all intents in the current conversation)
#
DOMAIN_FILE='../domain.yml'
import yaml
mongocmd=""" times_XXX: {
    $size: {
      $filter: {
        input: '$user_utterances',
        as: 'e',
        cond: {
          $in: [
            '$$e.parse_data.intent.name',
            [
              'XXX'
            ]
          ]
        }
      }
    }
  },"""
with open(DOMAIN_FILE,'r') as file:
    document = yaml.safe_load(file)
    print(type(document.items()))
    for key,value in document.items():
        if key=="intents":
            print(type(value[6]))
            print(type(value[4]))
            intent_list=[ x for x in value if isinstance(x,str) ]
            resp_sec_list = [ x for x in value if isinstance(x,dict) ]
            intent_list_cont= [ list(x.keys())[0] for x in resp_sec_list ]
            all_intents=intent_list+intent_list_cont
            print(all_intents)
            break
            
       
for intent in all_intents:
    cmd = mongocmd.replace('XXX', intent)
    print(cmd)
                 

