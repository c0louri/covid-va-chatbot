# 
# mongo_generate_analytics_coll.py
#
# input: dbname, collection with conversations, target collection, start date (optional) end date (optional, both in(dd/mm/yyy)
#
# output: target collection is generated, where conversations in the 
# specified period (if dates specified) or all conversations (otherwise) 
# are enriched with analytics data such as topics discussion counters and 
# conversation length. 
#
# example usage:
# python mongo_generate_analytics_coll.py rasadb conv conv_analytics
# (runs for all time)
# python mongo_generate_analytics_coll.py rasadb conv conv_analytics 20/1/2021
# (runs for conversations on 20/1/2021 onwards)
# python mongo_generate_analytics_coll.py rasadb conv conv_analytics 20/1/2021 25/1/2021
# (runs for conversations from 20/1/2021 to 25/1/2021 inclusive )



from pymongo import MongoClient
import pymongo
import datetime
import csv
import argparse

def str2unix(ts):
    d=datetime.datetime.strptime(ts,"%d/%m/%Y-%H:%M:%S")
    print(d)
    return d.timestamp()

# ************************************* const ********************************
MONGO_SERVICE_HOST="dialogue.ilsp.gr"
MONGO_SERVICE_PORT=8894
client = MongoClient(MONGO_SERVICE_HOST, MONGO_SERVICE_PORT)
colms= ['message','predicted_intent','actual_intent']
RESP_SEL=['ask_chitchat','mask_faq','EODY_faq','out_of_scope']
HELP="""# 
# mongo_generate_analytics_coll.py
#
# input: dbname, collection with conversations, target collection, start date (optional) end date (optional, both in(dd/mm/yyy)
#
# output: target collection is generated, where conversations in the 
# specified period (if dates specified) or all conversations (otherwise) 
# are enriched with analytics data such as topics discussion counters and 
# conversation length. 
#
# example usage:
# python mongo_generate_analytics_coll.py rasadb conv conv_analytics
# (runs for all time)
# python mongo_generate_analytics_coll.py rasadb conv conv_analytics 20/1/2021
# (runs for conversations on 20/1/2021 onwards)
# python mongo_generate_analytics_coll.py rasadb conv conv_analytics 20/1/2021 25/1/2021
# (runs for conversations from 20/1/2021 to 25/1/2021 inclusive )
"""

today_str=(datetime.date.today()).strftime('%d/%m/%Y')
parser = argparse.ArgumentParser(usage=HELP)
parser.add_argument("dbname", type=str, help="the db name")
parser.add_argument("src_col", type=str, help="the collection where the data is stored") 
parser.add_argument("dst_col", type=str, help="collection to push the extended data") 
parser.add_argument("start_date", type=str, nargs='?', default="1/1/2020", help="start date in unix ts")
parser.add_argument("end_date", type=str, nargs='?', default=today_str, help="end date in unix ts")
args=parser.parse_args()
print(args)

db = client[args.dbname]
c = db[args.src_col]
start_date= args.start_date+"-00:00:00"
end_date= args.end_date+"-23:59:59"
start=str2unix(start_date)
end=str2unix(end_date)

if not args.dbname in client.list_database_names():
    print("DB {} does not exist".format(args.dbname))
    exit()
elif not c.find_one():
    print("collection {}.{} does not exist".format(args.dbname,args.src_col))
    exit()
else: 
    print("Filtering collection '{}' from {} to {}, and storing the result in {}".format(args.src_col,start_date, end_date,args.dst_col))



stage_1= [ {"$match": { "latest_event_time" : {"$gt" : start, "$lt": end }}}]


stage_2=[{
        '$addFields': {
            'events_original': '$events'
            }
        }]
stage_3=[{
        '$project': {
            'slots': 1, 
            'sender_id': 1, 
            'events_original':1,
            'events': {
                '$filter': {
                    'input': '$events', 
                    'as': 'event', 
                    'cond': {
                        '$in': [
                            '$$event.event', [
                                'user', 'bot'
                            ]
                        ]
                    }
                }
            }
        }
    }]


clear_singleton_dial=[
    {
        '$addFields': {
            'keep_elements_that_have_user_events_before': {
                '$filter': {
                    'input': '$events', 
                    'as': 'e', 
                    'cond': {
                        '$toBool': {
                            '$let': {
                                'vars': {
                                    'prev': {
                                        '$arrayElemAt': [
                                            '$events', {
                                                '$subtract': [
                                                    {
                                                        '$indexOfArray': [
                                                            '$events', '$$e'
                                                        ]
                                                    }, 1
                                                ]
                                            }
                                        ]
                                    }
                                }, 
                                'in': {
                                    '$toBool': {
                                        '$in': [
                                            '$$prev.event', [
                                                'user'
                                            ]
                                        ]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }, {
        '$addFields': {
            'first_bot_responses': {
                '$filter': {
                    'input': '$keep_elements_that_have_user_events_before', 
                    'as': 'e', 
                    'cond': {
                        '$toBool': {
                            '$in': [
                                '$$e.event', [
                                    'bot'
                                ]
                            ]
                        }
                    }
                }
            }
        }
    }, {
        '$addFields': {
            'conversation_rounds': {
                '$size': '$first_bot_responses'
            }
        }
    },
    {"$match": { "conversation_rounds" : {"$gt" : 1}}}, 
    ]

pipeline=stage_1+ stage_2+stage_3+clear_singleton_dial
pipeline.append({"$out": args.dst_col})
c.aggregate(pipeline)
c = db[args.dst_col]

# TODO: update these
# topics list
t_l = ["icu_stats_form", "vaccine_stats_form", "covid_statistics_form", "pharmacy_form",
                            "ill_questionnaire_form", "ways_of_protection", "symptoms", "test_types",
                            "test_cost", "what_to_do_if_positive", "underlying_conditions", "no_action",
                            "feedback_form", "EODY_faq", "ask_chitchat", "mask_faq", "out_of_scope",
                            "positive_feedback", "insult"]


intent_to_functionality_d={
    "covid_stats" : "covid_statistics_form",
    "EODY_faq" : "EODY_faq",
    "ask_chitchat" : "ask_chitchat",
    "mask_faq" : "mask_faq",
    "time_declare" : "no_action",
    "pharma_search" : "pharmacy_form",
    "insult" : "insult",
    "affirmative" : "no_action",
    "negative" : "no_action",
    "greetings" : "no_action",   #maybe tell we haven't greeted ourselves
    "stop_executing" : "no_action",
    "region_declare" : "no_action",
    "goodbye" : "no_action",
    "thank_you" : "no_action",
    "didnt_understand" : "no_action",
    "am_i_ill" : "ill_questionnaire_form",
    "why_avoid_pharma" : "no_action",
    "what_to_do_if_positive" : "what_to_do_if_positive",
    "symptoms" : "symptoms",
    "whats_your_name" : "no_action",
    "ways_of_protection" : "ways_of_protection",
    "underlying_conditions" : "underlying_conditions",
    "personal_ways_of_protection" : "ways_of_protection",
    "collective_ways_of_protection" : "ways_of_protection",
    "all_ways_of_protection" : "ways_of_protection",
    "test_cost" : "test_cost",
    "pcr_test_cost" : "test_cost",
    "rapid_test_cost" : "test_cost",
    "all_test_cost" : "test_cost",
    "self_test" : "no_action",
    "all_of_them" : "no_action",
    "test_types" : "test_types",
    "general_knowledge_tests" : "no_action",
    "out_of_scope" : "out_of_scope",
    "vaccine_stats" : "vaccine_stats_form",
    "find_new_icus" : "icu_stats_form",
    "positive_feedback": "positive_feedback",
    "no_action" : "no_action"
}

collection_topic_count={}

for item in c.find():
    bot_has_suggested=""
    topic_count={}
    for topic in t_l:
      topic_count[topic]=0
    for e in item['events']:
        if e['event']=='user':
          intent = e["parse_data"].get("intent",{}).get("name")
          if intent is None:
            continue

          if e['parse_data']['intent']['confidence']<0.5:
            print(intent)

          if intent in intent_to_functionality_d:

            if intent=='affirmative' and bot_has_suggested!='':
              topic_count[bot_has_suggested]+=1
            else:
              topic_count[intent_to_functionality_d[intent]]+=1

            #as user answer has been considered, we no longer need this
          bot_has_suggested=''
        elif e['event']=='slot':
          if e['name']=='bot_suggestion':
              bot_has_suggested=""
              if e['value']!="no_action":
                 # now we must check if user says yes:
                 bot_has_suggested=e['value']
    topic_count.pop('no_action')
    item['topic_count']=topic_count
    itemid=item['_id']
    print('updating')
    c.update_one({'_id':itemid}, {"$set": item}, upsert=False)
