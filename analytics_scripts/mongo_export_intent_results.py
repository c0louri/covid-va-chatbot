# 
# mongo_export_intents.py
#
# input: dbname, collection with conversations, start date (optional) end date (optional, both in(dd/mm/yyy)
#
# output:  a file named <dbname>_intent_report.csv with all the intent predictions in 
# the given collection of conversations. Supports both rasa1 and rasa2+ trackrs.
# The format of the csv allows for annotating the actual_intent, so as to 
# generate the confusion matrices.
# Column names:
# 'message','predicted_intent','actual_intent' (set in constant CSV_COLMS)
#
# example usage:
# python mongo_export_intents.py rasadb conversations
# (runs for all time)
# python mongo_export_intents.py rasadb conversations 20/1/2021
# (runs for conversations on 20/1/2021 onwards)
# python mongo_export_intents.py rasadb conversations 20/1/2021 25/1/2021
# (runs for conversations from 20/1/2021 to 25/1/2021 inclusive )


from pymongo import MongoClient
import datetime
import pymongo
import csv
import argparse


# ************************* functions ****************
def str2unix(ts):
    d=datetime.datetime.strptime(ts,"%d/%m/%Y-%H:%M:%S")
    print(d)
    return d.timestamp()

def generate_intents_csv_rasa1(db_name,collection_name):
    db = client[db_name]
    c= db[collection_name]
    csv_items=[]
    if not c.find_one():
        # empty dicts evaluate to False
        print("Nothing in collection to export intents from. Exiting...")
        exit()
    for item in c.find():
        events = item["events"]
        usr_events= [x for x in events if x["event"]=="user"]
        user=[]
        for x in usr_events:
            try:
                intent=x["parse_data"]["intent"]["name"]
                if x["parse_data"]["intent"]["confidence"]<0.5:
                    intent="nlu_fallback"
                user.append((x["text"],
                       intent,
                       x["parse_data"]["response_selector"]["ask_chitchat"]["full_retrieval_intent"],
                       x["parse_data"]["response_selector"]["mask_faq"]["full_retrieval_intent"],
                       x["parse_data"]["response_selector"]["EODY_faq"]["full_retrieval_intent"],
                       x["parse_data"]["response_selector"].get("out_of_scope",{}).get("full_retrieval_intent")
                       ))
            except KeyError as e:
                print("Key error, skipping this conversation")
                break
        for utt in user:
            pred=utt[1]
            if pred in RESP_SEL:
                # change this line if you dislike '/'
                pred=utt[2+RESP_SEL.index(pred)]
            
            if pred is None:
                continue
            
            user_dict={ "message": utt[0], "predicted_intent": pred, "actual_intent": pred}
            csv_items.append(user_dict)
    
    csv_file = db_name+'_'+collection_name+".csv"
    try:
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=CSV_COLMS)
            writer.writeheader()
            for data in csv_items:
                writer.writerow(data)
        print("Done writing to "+ csv_file) 
    except IOError:
        print("I/O error")
    

def generate_intents_csv_rasa2(db_name,collection_name):
    db = client[db_name]
    c= db[collection_name]
    csv_items=[]
    if not c.find_one():
        # empty dicts evaluate to False
        print("Nothing in collection to export intents from. Exiting...")
        exit()
    for item in c.find():
        events = item["events"]
        usr_events=[x for x in events if x["event"]=="user"]
        # create csv lines now
        user=[]
        for x in usr_events:
            try:
                intent=x["parse_data"]["intent"]["name"]
                if x["parse_data"]["intent"]["confidence"]<0.5:
                    intent="nlu_fallback"

                # check if confidence was too low
                user.append((x["text"],
                   intent,
                   x["parse_data"]["response_selector"]["ask_chitchat"]["response"]["intent_response_key"],
                   x["parse_data"]["response_selector"]["mask_faq"]["response"]["intent_response_key"],
                   x["parse_data"]["response_selector"]["EODY_faq"]["response"]["intent_response_key"],
                   x["parse_data"]["response_selector"]["out_of_scope"]["response"]["intent_response_key"],
                   ))

            except KeyError:
                print("Key error for intent:")
                print(x["parse_data"]["intent"]["name"])
                print("Are you sure this is in rasa 2 format?")
                continue
                #sometimes bad document, ignore and move on

        for utt in user:
            pred=utt[1]
            if pred in RESP_SEL:
                # change this line if you dislike '/'
                pred=utt[2+RESP_SEL.index(pred)]
            if pred is None:
                continue

            user_dict={ "message": utt[0], "predicted_intent": pred, "actual_intent": pred}
            csv_items.append(user_dict)
    
    csv_file = db_name+'_'+collection_name+".csv"
    try:
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=CSV_COLMS)
            writer.writeheader()
            for data in csv_items:
                writer.writerow(data)
        print("Done writing to "+ csv_file) 

    except IOError:
        print("I/O error")


# ***************** constants ***************************

OUT_FILE_NAME="intent_report" # OUT_FILE_NAME is also to be used as the temporary collection name
MONGO_SERVICE_HOST="dialogue.ilsp.gr"
MONGO_SERVICE_PORT=8894
client = MongoClient(MONGO_SERVICE_HOST, MONGO_SERVICE_PORT)
CSV_COLMS= ['message','predicted_intent','actual_intent']
RESP_SEL=['ask_chitchat','mask_faq','EODY_faq','out_of_scope']
HELP="""# 
# mongo_export_intents.py
#
# input: dbname, collection with conversations, start date (optional) end date (optional, both in(dd/mm/yyy)
#
# output:  a file named intent_report.csv with all the intent predictions in 
# the given collection of conversations. Supports both rasa1 and rasa2+ trackrs.
# The format of the csv allows for annotating the actual_intent, so as to 
# generate the confusion matrices.
# Column names:
e'message','predicted_intent','actual_intent' (set in constant CSV_COLMS)
#
# example usage:
# python mongo_export_intents.py rasadb conversations
# (runs for all time)
# python mongo_export_intents.py rasadb conversations 20/1/2021
# (runs for conversations on 20/1/2021 onwards)
# python mongo_export_intents.py rasadb conversations 20/1/2021 25/1/2021
# (runs for conversations from 20/1/2021 to 25/1/2021 inclusive )
"""

#*********** parse args *************************
parser = argparse.ArgumentParser(usage=HELP)
parser.add_argument("--rasa1", help="handle rasa 1 event trackers",
                            action="store_true")
parser.add_argument("dbname", type=str, help="the db name")
parser.add_argument("src_col", type=str, help="the collection where the data is stored") 
parser.add_argument("start_date", type=str, nargs='?', default="1/1/2020", help="start date in unix ts")
today_str=(datetime.date.today()).strftime('%d/%m/%Y')
parser.add_argument("end_date", type=str, nargs='?', default=today_str, help="end date in unix ts")
args=parser.parse_args()
print("Got cmd args: " + str(args))

db = client[args.dbname]
c = db[args.src_col]

start_date= args.start_date+"-00:00:00"
end_date= args.end_date+"-23:59:59"
start=str2unix(start_date)
end=str2unix(end_date)

if not args.dbname in client.list_database_names():
    print("DB {} does not exist".format(args.dbname))
    exit()
elif not c.find_one():
    print(args.src_col + " is empty. Exiting...")
    exit()
else: 
    print("Exporting intents from collection '{}' from {} to {}, and storing the results in {}.csv".format(args.src_col, start_date, end_date, args.dbname+'_'+OUT_FILE_NAME))


# store the filtering results into a temporary collection named 
# OUT_FILE_NAME
p = [ {"$match": { "latest_event_time" : {"$gt" : start, "$lt": end }}}, 
      {"$out": OUT_FILE_NAME},
    ]
c.aggregate(p)
if not db["OUT_FILE_NAME"].find():
    print("No data found in given period, " + start_date + "-" + end_date)
    exit()

# created conversations_p1, now to get the info
if args.rasa1:
    generate_intents_csv_rasa1(args.dbname,OUT_FILE_NAME)
else:
    generate_intents_csv_rasa2(args.dbname,OUT_FILE_NAME)
