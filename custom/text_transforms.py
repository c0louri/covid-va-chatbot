import re
import typing
import unicodedata
from functools import lru_cache
from typing import Any, Dict, List, Optional, Text, Tuple, Type

import requests
from rasa.nlu.components import Component
from rasa.nlu.config import InvalidConfigError, RasaNLUModelConfig
from rasa.nlu.model import Metadata
from rasa.nlu.tokenizers.tokenizer import Token
from rasa.shared.nlu.training_data.training_data import Message, TrainingData

from .constants import ALLOWED_CHARACTERS

if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata


class BaseTextTransformation(Component):
    """Does not require any kind of featurization."""

    requires = []
    provides = ["text"]

    @classmethod
    def required_packages(cls):
        return []

    def __init__(self, component_config: Dict[Text, Any] = None) -> None:
        super(BaseTextTransformation, self).__init__(component_config)

        self.monitor_counter = 0

    def train(
        self, training_data: TrainingData, cfg: RasaNLUModelConfig, **kwargs: Any
    ) -> None:
        """Not a trainable component."""

        for example in training_data.intent_examples:
            transformed_text = self.transform_text(example)
            example.set("text", transformed_text, add_to_output=True)

        return None

    def persist(self, file_name: Text, model_dir: Text) -> Optional[Dict[Text, Any]]:
        """Nothing to persist here."""

        return None

    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Optional[Text] = None,
        model_metadata: Optional[Metadata] = None,
        cached_component: Optional[Component] = None,
        **kwargs: Any,
    ) -> Component:

        return cls(meta)

    def transform_text(self, message: Message) -> Text:
        raise NotImplementedError

    def process(self, message: Message, **kwargs: Any) -> None:
        """Replace text in message with transformation."""

        if "text" in message.data:
            transformed_text = self.transform_text(message)
            message.set("text", transformed_text, add_to_output=True)

        print("Changer counter: {}".format(self.monitor_counter))

        self.monitor_counter += 1


class DebugTextTransformation(BaseTextTransformation):
    def transform_text(self, message: Message) -> Text:
        text = message.data["text"]
        transformed_text = text + " mia wraia petalouda"

        return transformed_text


@lru_cache(maxsize=10000)
def clean_text(text):
    transformed_text = unicodedata.normalize("NFD", text)
    transformed_text = "".join(
        c if c in ALLOWED_CHARACTERS else " " for c in transformed_text
    )
    transformed_text = re.sub(r"\s\s+", " ", transformed_text.strip())
    transformed_text = unicodedata.normalize("NFC", transformed_text)

    return transformed_text


class TextCleaner(BaseTextTransformation):
    def transform_text(self, message: Message) -> Text:
        return clean_text(message.data["text"])


@lru_cache(maxsize=10000)
def spell_check(text, url):
    res = requests.post("{}/correct".format(url), json={"sentence": text})
    # Returns 5 most probable corrections. Select first....
    transformed_text = res.json()["correct"][0]

    return transformed_text


class SpellChecker(BaseTextTransformation):
    def transform_text(self, message: Message) -> Text:
        return spell_check(message.data["text"], self.component_config["url"])


@lru_cache(maxsize=10000)
def greeklish2greek(text, url):
    transformed_text=text
    try:
        res = requests.post("{}/greeklish".format(url), json={"sentence": text})
        transformed_text = res.json()["sentence"]
    except:
        print("Failed to convert greeklish to greek, perhaps service is down?")


    return transformed_text


class Greeklish2Greek(BaseTextTransformation):
    def transform_text(self, message: Message) -> Text:
        return greeklish2greek(message.data["text"], self.component_config["url"])
