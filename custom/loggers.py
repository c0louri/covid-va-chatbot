import typing
from typing import Any, Dict, List, Optional, Text, Tuple, Type

from rasa.nlu.components import Component
from rasa.nlu.config import InvalidConfigError, RasaNLUModelConfig
from rasa.nlu.model import Metadata
from rasa.nlu.tokenizers.tokenizer import Token
from rasa.shared.nlu.training_data.training_data import Message, TrainingData

if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata


def _is_list_tokens(v):
    """
    This is a helper function.
    It checks if `v` is a list of tokens.
    If so, we need to print differently.
    """

    if isinstance(v, List):
        if len(v) > 0:
            if isinstance(v[0], Token):
                return True

    return False


class BaseLogger(Component):
    @classmethod
    def required_components(cls) -> List[Type[Component]]:
        return []

    defaults = {"alias": None}
    language_list = None

    def __init__(self, component_config: Optional[Dict[Text, Any]] = None) -> None:
        super().__init__(component_config)

        self.monitor_counter = 0

    def train(
        self,
        training_data: TrainingData,
        config: Optional[RasaNLUModelConfig] = None,
        **kwargs: Any,
    ) -> None:
        pass

    def log(self, text: str):
        raise NotImplementedError

    def process(self, message: Message, **kwargs: Any) -> None:
        if self.component_config["alias"]:
            self.log("\n")
            self.log(self.component_config["alias"])

        for k, v in message.data.items():
            if _is_list_tokens(v):
                self.log(f"{k}: {[t.text for t in v]}")
            else:
                self.log(f"{k}: {v.__repr__()}")

        self.monitor_counter += 1

        print("Logger counter: {}".format(self.monitor_counter))

    def persist(self, file_name: Text, model_dir: Text) -> Optional[Dict[Text, Any]]:
        pass

    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Optional[Text] = None,
        model_metadata: Optional["Metadata"] = None,
        cached_component: Optional["Component"] = None,
        **kwargs: Any,
    ) -> "Component":
        """Load this component from file."""

        if cached_component:
            return cached_component
        else:
            return cls(meta)



class Printer(BaseLogger):
    def log(self, text: str):
        print(text)


class FileLogger(BaseLogger):
    # TODO: Needs work to function properly.

    defaults = {"alias": None, "filename": "rasa.output.log"}
    def __init__(self, component_config: Optional[Dict[Text, Any]] = None) -> None:
        super().__init__(component_config)
        self.file_descriptor = open(self.component_config["filename"], "a+")

    def __del__(self):
        self.file_descriptor.close()

    def log(self, text: str):
        self.file_descriptor.write("{}\n".format(text))



class DatabaseLogger(BaseLogger):
    # TODO: @kosmas we can implement if necessary
    pass
