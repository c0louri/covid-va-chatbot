# rasa custom components

We have added the greeklish2greek component, which
is making use of a microservice running 
at http://dialogue3:8892. The service is 
dockerized, and the file is hosted on 
dockerhub as georgepar/allgreek2me.ilsp.gr:latest.
Also to make this work, we have added a character cleaner, 
(to remove weird characters and perform unicode normalization) 
as well as a logger component, to be used for debugging, at least 
in the beginning.

To set launch it, use a compose file such as the following
```
version: "3.0"
services:
  allgreek2me:
    image: georgepar/allgreek2me.ilsp.gr:latest
    ports:
      - "8892:4444"
```
then run `docker-compose up allgreek2me` in the directory where the compose file is located.

On the chatbot's side,  to make this work,  we need to 
* add the code in the `custom` directory and
* add the components in the nlu pipeline, in the correct place.
* finally, train the chatbot.
                         


