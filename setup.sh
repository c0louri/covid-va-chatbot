git clone https://gitlab.com/ilsp-spmd-all/dialogue/covid-va.git
cd covid-va
git config credential.helper store
git submodule init
git submodule update
cd dialog
git config credential.helper store
git checkout master
cd ..
cp docker-compose-override-generic.yml docker-compose.override.yml
# edit docker-compose.override.yml to set
# port number, as well name of mongo db, 
# and rabbit 
vi docker-compose.override.yml
# clone covid-19 repo:
git clone  --no-checkout --depth 1 https://github.com/owid/covid-19-data
cd covid-19-data
git checkout master
cd ..
# edit cron jobs to work in current machine (find them in update_jobs)
# and add them to /etc/cron.d/covid-va-update (find sample in update_jobs)
# next, run the two scripts to get the latest data once:
source compute_update_va_nyrros.sh # replace with own script
source compute_update_owid_repo.sh # replace with own script
# finally, up the services
docker-compose up rasa rasa_actions redis
