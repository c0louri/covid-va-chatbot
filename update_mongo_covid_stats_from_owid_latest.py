import sys
import pandas as pd
OWID_FILE_PATH=sys.argv[1]
DATE=sys.argv[2]
print("storing for date " +DATE)

import os
from scraping.documents import covid_api_data
from datetime import datetime
import mongoengine as me

MONGO_SERVICE_HOST = os.environ["MONGO_SERVICE_HOST"] if "MONGO_SERVICE_HOST" in os.environ else "localhost"
MONGO_SERVICE_PORT = int(
    os.environ['MONGO_SERVICE_PORT']) if "MONGO_SERVICE_PORT" in os.environ else 27017

print("Logging to the API.")


'''
The data will be saved on the same document inside a dict field.
The outter dict will have a datetime for the date the data were scraped
and inside dict will have all the info that the covid api provides 
(maybe will exclude the longtitude,lattitude,time)

The datetime object will be stringified with a default method.
'''

print("Getting the current data...")
#read all the data for today

print("Connecting to the db")
me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)

#current date in format day/month/year
str_date = datetime.utcnow().strftime("%d/%m/%Y")

#the first dict is for the world without a country name
for covid_obj in covid_api_data.objects():
    country=covid_obj['English']
    owid_cases_data = pd.read_csv(OWID_FILE_PATH)
    owid_cases_data = owid_cases_data.fillna(0)
    data = owid_cases_data.loc[owid_cases_data["location"] == country]
    if data.empty:
        continue
    data= data.to_dict()
    up={}
    up['confirmed']=int(list(data['total_cases'].values())[0])
    up['deaths'] = int(list(data['total_deaths'].values())[0])
    up['new_cases'] = int(list(data['new_cases'].values())[0])
    up['new_deaths']= int(list(data['new_deaths'].values())[0])
    print(up)
    daily_data=covid_obj.covid_api_info_dict
    daily_data[DATE]=up
    covid_obj.save()
     
"""
for new_country in all_data_dict[1::]:
    owid_cases_data = pd.read_csv(OWID_FILE_PATH)
    owid_cases_data = owid_cases_data.fillna(0)
    data = owid_cases_data.loc[owid_cases_data["location"] == new_country]
    name = new_country["country"]
    
    #the names of the countries are saved in the DB
    covid_info_obj = covid_api_data.objects(English = name)
    
    #if the db doesnt respond to the query it will return an empty list obj
    if len(covid_info_obj) == 0:
        print("Error: the DB cannot be updated because the greek2english collection is not properly initialized! Have you run the init scripts?")
    else:        
        obj_to_save = covid_api_data()
        
        #objects primary key
        obj_to_save.pk = covid_info_obj[0].pk
        
        #fetching the previous version of the dictionary
        temp = covid_info_obj[0].covid_api_info_dict
        
        #cast to a dictionary from a BaseDict object
        temp = dict(temp)
        
        #Decimal object cannot be encoded in the DB, thus pop them
        new_country.pop("country")
        new_country.pop("total_tests_per_million")
        new_country.pop("total_cases_per_million")
        new_country.pop("total_deaths_per_million")
        
        #update the Decimal population of the country with the int equivalent
        new_country.update({"population" : int(new_country["population"])})
        
        #update it with the new dict
        temp.update({str_date : new_country})
        
        #save it to the new obj_to_save
        obj_to_save.covid_api_info_dict = temp
        
        #save the object
        obj_to_save.save()
        print(obj_to_save.covid_api_info_dict)
        
        print("Data from {} saved!".format(covid_info_obj[0].Greek))
  
"""
