# 
# nlu_difference.py
# 
# usage: python nlu_difference.py <new_nlu> <old_nlu>

import sys
import yaml
old_nlu=sys.argv[2]
new_nlu=sys.argv[1]
intents = []

def Diff(li1, li2):
    return list(set(li1) - set(li2))

def gi(f):
    res={}
    with open(f,'r') as file:
        document = yaml.safe_load(file)
        print(type(document.items()))
        for key,value in document.items():
            if key=="nlu":
               for intent in value:
                   try:
                    inte=intent['intent']
                    examples=ymlarraystring2list(intent['examples'])
                    i=0
                    for ex in examples:
                        examples[i]=ex.strip()
                        i+=1
                    res[inte]=examples
                    intents.append(inte)
                   except KeyError as e:
                    # print(intent)
                    pass
        return res


def ymlarraystring2list(s):
    # turn s into a python list
    return s.split('- ')
new=gi(new_nlu)
old=gi(old_nlu)
for key,value in new.items():
    print("************************************")
    print("For intent: {}".format(key))
    print("************************************")
    if not key in old:
        print("{} not in old nlu file".format(key))
    else:
        print(Diff(new[key],old[key]))

