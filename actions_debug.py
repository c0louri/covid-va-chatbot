import logging
import traceback
from datetime import datetime

logger = logging.getLogger(__name__)
from typing import Any, Text, Dict, List
from rasa_sdk.events import SessionStarted, ActionExecuted
from rasa_sdk.interfaces import Action, ActionExecutionRejection
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, EventType, ConversationPaused
from rasa_sdk import FormValidationAction
from rasa_sdk.types import DomainDict

REQUESTED_SLOT = "requested_slot"

from typing import Any, Text, Dict, List
import os
from random import sample, uniform
MONGO_SERVICE_HOST = os.environ["MONGO_SERVICE_HOST"] if "MONGO_SERVICE_HOST" in os.environ else "localhost"
MONGO_SERVICE_PORT = int(
    os.environ['MONGO_SERVICE_PORT']) if "MONGO_SERVICE_PORT" in os.environ else 27017

NYRROS_FILE_PATH = os.environ["NYRROS_FILE_PATH"] if "NYRROS_FILE_PATH" in os.environ else "./data/external_data_sources/covid_19gr_nyrros.xlsx"

OWID_FILE_PATH = os.environ["OWID_FILE_PATH"] if "OWID_FILE_PATH" in os.environ else "./data/external_data_sources/vaccinations/country_data/Greece.csv"
ARTICLES_JSON_PATH = os.environ["ARTICLES_JSON_PATH"] if "ARTICLES_JSON_PATH" in os.environ else "data/countries_with_article.json"
TOTAL_COVID_ICUS = 687
REDIS_SERVICE_HOST= os.environ["REDIS_SERVICE_HOST"] if "REDIS_SERVICE_HOST" in os.environ else "localhost"
REDIS_SERVICE_PORT= os.environ["REDIS_SERVICE_PORT"] if "REDIS_SERVICE_PORT" in os.environ else 6379
REDIS_DB= 0
NO_ACTION_PROB= float(os.environ["NO_ACTION_PROBABILITY"]) if "NO_ACTION_PROBABILITY" in os.environ else 0.2
BOT_STATES_TERMS_AT_BEGINNING = True

intent_to_functionality_dict = {
    "covid_stats" : "covid_statistics_form",
    "EODY_faq" : "no_action",
    "ask_chitchat" : "no_action",
    "mask_faq" : "no_action",
    "time_declare" : "no_action",
    "pharma_search" : "pharmacy_form",
    "insult" : "no_action",
    "affirmative" : "no_action",
    "negative" : "no_action",
    "greetings" : "no_action",   #maybe tell we haven't greeted ourselves
    "stop_executing" : "no_action",
    "region_declare" : "no_action",
    "goodbye" : "no_action",
    "thank_you" : "no_action",
    "didnt_understand" : "no_action",
    "am_i_ill" : "ill_questionnaire_form",
    "why_avoid_pharma" : "no_action",
    "what_to_do_if_positive" : "what_to_do_if_positive",
    "symptoms" : "symptoms",
    "whats_your_name" : "no_action",
    "ways_of_protection" : "ways_of_protection",
    "underlying_conditions" : "underlying_conditions",
    "personal_ways_of_protection" : "ways_of_protection",
    "collective_ways_of_protection" : "ways_of_protection",
    "all_ways_of_protection" : "ways_of_protection",
    "test_cost" : "test_cost",
    "pcr_test_cost" : "test_cost",
    "rapid_test_cost" : "test_cost",
    "all_test_cost" : "test_cost",
    "self_test" : "no_action",
    "all_of_them" : "no_action",
    "test_types" : "test_types",
    "general_knowledge_tests" : "no_action",
    "out_of_scope" : "no_action",
    "vaccine_stats" : "vaccine_stats_form",
    "find_new_icus" : "icu_stats_form",
    "positive_feedback": "no_action",
    "affirmative+covid_stats" : "covid_statistics_form",
    "affirmative+pharma_search" : "pharmacy_form",
    "affirmative+find_new_icus" : "icu_stats_form",
    "affirmative+vaccine_stats" : "vaccine_stats_form",
    "negative+covid_stats" : "covid_statistics_form",
    "negative+pharma_search" : "pharmacy_form",
    "negative+find_new_icus" : "icu_stats_form",
    "negative+vaccine_stats" : "vaccine_stats_form",
    "negative+thank_you" : "no_action",
    "greetings+covid_stats" : "covid_statistics_form",
    "greetings+pharma_search" : "pharmacy_form",
    "greetings+ask_chitchat" : "no_action",
    "thank_you+covid_stats" : "covid_statistics_form",
    "greetings+whats_your_name" : "no_action",
    "no_action" : "no_action"
}

list_of_functionalities = ["icu_stats_form", "vaccine_stats_form", "covid_statistics_form", "pharmacy_form",
                            "ill_questionnaire_form", "ways_of_protection", "symptoms", "test_types",
                            "test_cost", "what_to_do_if_positive", "underlying_conditions", "no_action"]

functionality_proposal = {"covid_statistics_form" : "Θα ήθελες να μάθεις για τα κρούσματα; Έχω πληροφορίες για την Ελλάδα και τον υπόλοιπο κόσμο.",
                    "pharmacy_form" : "Γνωρίζω και πληροφορίες για φαρμακεία! Μήπως θέλεις να σου βρω κάποιο;",
                    "ill_questionnaire_form" : "Μήπως θα ήθελες να σου κάνω κάποιες ερωτήσεις εν είδει διαγνωστικού τεστ; Μην ξεχνάς ότι δεν είμαι γιατρός όμως!",
                    "ways_of_protection" : "Θέλεις να σε ενημερώσω σχετικά με τα μέτρα πρόληψης;",
                    "test_cost" : "Θα ήθελες να μάθεις πόσο κοστίζουν τα τεστ για τον κορωνοϊό;",
                    "test_types" : "Θα ήθελες να μάθεις ποια είναι τα διαφορετικά είδη των τεστ;",
                    "what_to_do_if_positive": "Θέλεις να σου πω τι πρέπει να κάνεις σε περίπτωση που είσαι φορέας;",
                    "underlying_conditions": "Γνωρίζω και σχετικά με τα υποκείμενα νοσήματα, αν σε ενδιαφέρει να μάθεις.",
                    "symptoms": "Να σου πω ποια είναι τα κύρια συμπτώματα του κορωνοϊού;",
                    "vaccine_stats_form" : "Θέλεις να μάθεις για την εξέλιξη των εμβολιασμών στην Ελλάδα;",
                    "icu_stats_form" : "Ξέρω και για τις ΜΕΘ. Θέλεις να μάθεις για τη διαθεσιμότητα που υπάρχει;",
                    "no_action" : "Τα έχουμε πει όλα." #Or sth similar
                    }

class pharmacy_form_submit(Action):
    def name(self):
        return "pharmacy_form_submit"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        from bs4 import BeautifulSoup
        import requests
        import mongoengine as me
        from scraping import City, url_fetch

        #Region user preference
        region = tracker.get_slot("requested_city")

        #It provides a list because first, it is an extracted entity from the CRFExtractor
        #and second it is a result from the LUT.
        if (type(region) == list):
            region = region[0]
        try:
            me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
        except pymongo.errors.ServerSelectionTimeoutError as tout:
            print(tout)
            dispatcher.utter_message(text="Δυστυχώς δεν μπορώ να συνδεθώ με την βάση δεδομένων αυτή τη στιγμή!")
            return None

        url = url_fetch(region)

        if url == None:
            dispatcher.utter_message(text="Δεν μπόρεσα να βρω την συγκεκριμένη πόλη. Μπορείς να επαναλάβεις ή να αναζητήσεις φαρμακείο σε κάποια άλλη περιοχη;")
            return []

        #pass a header to the request (form JSON str)
        agent = {"User-Agent":"Mozilla/5.0"}

        #Get request to Vrisko server and return a string var
        source = requests.get(url, headers=agent).text

        #soup-ify the str var
        soup = BeautifulSoup(source, 'lxml')

        #Exploit the advantage tha all the resuls are in a <a> tag with class 'ResultName'
        #and all addreses are in a <div> with class 'ResultAddr'
        #God bless the dev team of vrisko

        names = []
        addresses = []
        
        for name in soup.find_all('a', class_ = 'ResultName'):
            names.append(name.text[1::])
        
        num_of_pharmacies = len(names)
        if (num_of_pharmacies == 0):
            dispatcher.utter_message(text="Δυστυχώς δεν βρέθηκαν ανοιχτά φαρμακεία στη συγκριμένη πόλη.")
            return [SlotSet("requested_city", None)]
            

        for address in soup.find_all('div', class_ = 'ResultAddr'):
            temp_addr = address.text.replace('\n', "")
            temp_addr = temp_addr.strip()

            #Remove the Region from the end as Kosmas asked
            sub = temp_addr.split(",")[-1]
            temp_addr = temp_addr.replace(sub, "")
            
            #Change the last comma with a full stop
            temp_addr = temp_addr[0 : len(temp_addr) - 1]


            #splitting the postal code in half for better handling from TTS
            temp_addr = temp_addr[0:len(temp_addr) - 3] + " " + temp_addr[len(temp_addr) - 3::]

            addresses.append(temp_addr)
        
        if len(names) == 1:
            dispatcher.utter_message(text="Βρήκα 1 εφημερεύον φαρμακείο.")
            dispatcher.utter_message(text="Συγκεκριμένα:")
        else:
            dispatcher.utter_message(text="Βρήκα {} εφημερεύοντα φαρμακεία.".format(len(names)))
            dispatcher.utter_message(text="Συγκεκριμένα:")

        total_pharma_num=len(names)
        for i in range(len(names)):
            # if this is the last result to be dislayed
            if (i > 2) or (i == total_pharma_num - 1):
                dispatcher.utter_message(text = "και {} με διεύθυνση {}.".format(names[i],addresses[i]))
            else:
                dispatcher.utter_message(text = "{} με διεύθυνση {}, ".format(names[i],addresses[i]))

            #Getting only the first four results
            #We will need to post the results on a DB collection for them to be reused and not overuse the scraping from Vrisko.
            if i > 2:
                break
        
        #disconnect from the DB
        me.disconnect("default")

        return [SlotSet("requested_city", None)]

class validate_ill_questionnaire_form(FormValidationAction):
    def name(self) -> Text:
        return "validate_ill_questionnaire_form"

    def validate_has_cough(self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
        ) -> List[Dict]:

        if value == "not_understand":
            dispatcher.utter_message(text = "Εννοώ αν βήχεις παραπάνω από το φυσιολογικό αυτές τις μέρες.")
            return {"has_cough": None}
        
        return{"has_cough": value}

    def validate_has_fever(self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
        ) -> List[Dict]:

        if value == "not_understand":
            dispatcher.utter_message(text = "Εννοώ αν μέτρησες την θερμοκρασία σου και ήταν παραπάνω από το φυσιολογικό.")
            return {"has_fever": None}
        
        return{"has_fever": value}

    def validate_has_anosia(self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
        ) -> List[Dict]:


        if value == "not_understand":
            dispatcher.utter_message(text = "Εννοώ αν έχασες την αίσθηση της μυρωδιάς.")
            return {"has_anosia": None}
        
        return{"has_anosia": value}

    def validate_has_ageusia(self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
        ) -> List[Dict]:

        if value == "not_understand":
            dispatcher.utter_message(text = "Εννοώ αν έχασες την αίσθηση της γεύσης ή μερικών γεύσεων.")
            return {"has_ageusia": None}
        
        return{"has_ageusia": value}

    def validate_has_problem_breathing(self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
        ) -> List[Dict]:

        if value == "not_understand":
            dispatcher.utter_message(text = "Εννοώ αν λαχανιάζεις ή δυσκολεύεσαι στην αναπνοή.")
            return {"has_problem_breathing": None}

        return{"has_problem_breathing": value}

class ill_questionnaire_form_submit(Action):
    '''
    Simply give a diagnosis on the given symptoms.
    '''
    def name(self):
        return "ill_questionnaire_form_submit"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        cough = tracker.get_slot("has_cough")
        fever = tracker.get_slot("has_fever")
        short_breath = tracker.get_slot("has_problem_breathing")
        anosia = tracker.get_slot("has_anosia")
        ageusia = tracker.get_slot("has_ageusia")

        if anosia or ageusia:
            dispatcher.utter_message(text="Σύμφωνα με τα στοιχεία που μου έδωσες είναι πιθανό να είσαι φορέας. Θα σου πρότεινα να επικοινωνήσεις με τον γιατρό σου ή με τον ΕΟΔΥ, άλλωστε εγώ δεν είμαι γιατρός!")
        elif cough or fever or short_breath:
            dispatcher.utter_message(text="Υπάρχει περίπτωση να έχεις κάποια κοινή ίωση. Θα σου πρότεινα, όμως, να επικοινωνήσεις με τον γιατρό σου ή με τον ΕΟΔΥ, άλλωστε εγώ δεν είμαι γιατρός!")
        else:
            dispatcher.utter_message(text="Σύμφωνα με τα στοιχεία που μου έδωσες δεν είναι πιθανό να είσαι φορέας. Βέβαια, αν δεν αισθάνεσαι καλά θα σου πρότεινα να επικοινωνήσεις με τον γιατρό σου ή με τον ΕΟΔΥ, άλλωστε εγώ δεν είμαι γιατρός!")
        return [SlotSet("has_ageusia", None),
                SlotSet("has_anosia", None),
                SlotSet("has_problem_breathing", None),
                SlotSet("has_cough", None),
                SlotSet("has_fever", None)]

class Refresh_Am_I_ill_slot(Action):
    def name(self):
        return "action_refresh_slot_am_i_ill"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
            return [SlotSet("has_ageusia", None),
                SlotSet("has_anosia", None),
                SlotSet("has_problem_breathing", None),
                SlotSet("has_cough", None),
                SlotSet("has_fever", None)]


class action_refresh_covid_statistics_form(Action):
    def name(self):
        return "action_refresh_covid_statistics_form"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        return [SlotSet("brief_time", None),
                SlotSet("requested_city", None)]

class validate_covid_statistics_form(FormValidationAction):
    def name(self) -> Text:
        return "validate_covid_statistics_form"

    def validate_brief_country(self,
            value: Text,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
            ) -> List[Dict]:
            
            """
            We assume that the value of the slot will be valid (one of the countries provided in the LookUp Table)
            Greece will be a default value and will be treated specially!
            """

            if (value == "_NOT_EXTRACTED_"):
                dispatcher.utter_message(text = "Μήπως θα μπορούσες να επαναλάβεις; Δυστυχώς δεν κατάλαβα για ποια χώρα ενδιαφέρεσαι.")
                return {"brief_country" : None}
            else:
                return {"brief_country": value}
                
    def validate_brief_time(self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
        ) -> List[Dict]:
        ''' Validates slot brief_time. Checks if entity extraction has failed,
        by using the special value "_NOT_EXTRACTED_" which is assigned in
        meth:`~Greece_report_form.validate` '''
        logger.debug("Validating extracted slot brief_time: {}".format(value))
        if value=="_NOT_EXTRACTED_":
            return  { "brief_time" : None}
        else:
            time = value.split("T")[0]
            try:
                time = datetime.strptime(time, "%Y-%m-%d")
            except ValueError as v:
                logging.error(v)
                traceback.print_exc()
                logging.error("Error grabbing the time from entity")
                return { "brief_time" : None}

            return { "brief_time" : value}

class covid_statistics_form_submit(Action):
    def name(self):
        return "covid_statistics_form_submit"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        from bs4 import BeautifulSoup
        import requests
        import mongoengine as me
        from covid import Covid
        from scraping.documents import covid_api_data
        from datetime import datetime, timedelta
        from scraping.documents import EODY_briefing
        import json

        country_name = tracker.get_slot("brief_country")
        
        if (country_name != "Ελλάδα"):
            me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
            if (type(country_name) == list):
                country_name = country_name[0]
            
            obj = covid_api_data.objects(Greek = country_name)
            
            #get the dictionary, choose the date that you want
            #get the date from time
            declared_date = tracker.get_slot("brief_time")
            
            #it is a string, making it a datetime object (Year,Month,Day)
            temp = declared_date.split("T")[0]
            temp = temp.split("-")
            declared_date = datetime(int(temp[0]), int(temp[1]), int(temp[2]))
            declared_date_prior = declared_date - timedelta(days = 1)
            
            declared_date = declared_date.strftime("%d/%m/%Y")
            declared_date_prior = declared_date_prior.strftime("%d/%m/%Y")
            
            all_dates_dict = obj[0].covid_api_info_dict
            
            msg_output = ""
            date_output = datetime(1,1,1,1,1,1,1,None)

            #read dict with articles and countries
            with open(ARTICLES_JSON_PATH, "r") as f:
                c2a_dict = json.load(f)

            if ((declared_date in all_dates_dict.keys()) and (declared_date_prior in all_dates_dict.keys())):
                total_cases = all_dates_dict[declared_date]["confirmed"]
                new_cases = all_dates_dict[declared_date]["confirmed"] - all_dates_dict[declared_date_prior]["confirmed"]
                total_deaths = all_dates_dict[declared_date]["deaths"]
                new_deaths = all_dates_dict[declared_date]["deaths"] - all_dates_dict[declared_date_prior]["deaths"]
                
                if (country_name == "Παγκόσμια"):
                    msg_output = "Σύμφωνα με την ημερομηνία που ζήτησες ({}) υπήρξαν παγκοσμίως {} νέα κρούσματα με συνολικό αριθμό κρουσμάτων {}, {} νέους θανάτους και {} θανάτους συνολικά. ".format(
                        declared_date, new_cases, total_cases, new_deaths, total_deaths)
                else:
                    msg_output = "Σύμφωνα με την ημερομηνία που ζήτησες ({}) για {} υπήρξαν {} νέα κρούσματα με συνολικό αριθμό κρουσμάτων {}, {} νέους θανάτους και {} θανάτους συνολικά. ".format(
                        declared_date, c2a_dict.get(country_name,country_name), new_cases, total_cases, new_deaths, total_deaths)
                
                # 258 bug: negative deaths and cases.
                if (new_cases<0 or new_deaths <0) and (country_name == "Παγκόσμια"):
                    msg_output= "Για την ημερομηνία που ζήτησες ({}) δεν υπάρχουν αρκετά δεδομένα για παγκόσμια νέα κρούσματα και θανάτους, αλλά είχαμε συνολικά {} κρούσματα {} θανάτους. ".format(declared_date, total_cases, total_deaths)
                elif (new_cases<0 or new_deaths <0):
                    msg_output= "Για την ημερομηνία που ζήτησες ({}) για {} δεν υπάρχουν αρκετά δεδομένα για νέα κρούσματα και θανάτους, αλλά είχαμε συνολικά {} κρούσματα και {} θανάτους. ".format(declared_date, c2a_dict.get(country_name,country_name), total_cases, total_deaths)
                   
            else:
                if declared_date in all_dates_dict.keys():
                    data = all_dates_dict[declared_date]
                    date_output = declared_date
                    if (country_name == "Παγκόσμια"):
                        msg_output = "Δεν βρήκα πληροφορίες σχετικά με τα παγκόσμια στατιστικά στις {}, όμως τα συνολικά κρούσματα είναι {}, ενώ οι συνολικοί θάνατοι {}.".format(declared_date,all_dates_dict[declared_date]["confirmed"],all_dates_dict[declared_date]["deaths"])
                    else:
                        msg_output = "Για {} δεν βρήκα πληροφορίες σχετικά με τα στατιστικά στις {}, όμως τα συνολικά κρούσματα είναι {}, ενώ οι συνολικοί θάνατοι {}.".format(c2a_dict.get(country_name,country_name), declared_date,all_dates_dict[declared_date]["confirmed"],all_dates_dict[declared_date]["deaths"])
                else:
                    avalaible_dates = all_dates_dict.keys()

                    latest_available_day = datetime(2005,1,1)

                    for date in avalaible_dates:
                        split_str = date.split("/")
                        datetime_date = datetime(int(split_str[2]), int(split_str[1]), int(split_str[0]))

                        latest_available_day = datetime_date if datetime_date > latest_available_day else latest_available_day

                    latest_available_day = latest_available_day.strftime("%d/%m/%Y")

                    date_output = latest_available_day
                    data = all_dates_dict[latest_available_day]
                    total_cases = data["confirmed"]
                    new_cases = data["new_cases"]
                    total_deaths = data["deaths"]
                    new_deaths = data["new_deaths"]
                    msg_output = "Δεν γνωρίζω για τις {}, αλλά θα σου πω τα τελευταία γνωστά δεδομένα. ".format(declared_date)
                    msg_output = msg_output + "Στις {} συνολικά υπήρξαν {} νέα κρούσματα με συνολικό αριθμό κρουσμάτων {}, {} νέους θανάτους και {} θανάτους συνολικά. ".format(
                        date_output, new_cases, total_cases, new_deaths, total_deaths)
            
            dispatcher.utter_message(text=msg_output)
        else:
            
            declared_date = tracker.get_slot("brief_time")

            temp = declared_date.split("T")[0]
            temp = temp.split("-")
            declared_date = datetime(int(temp[0]), int(temp[1]), int(temp[2]))
            declared_date_str = declared_date.strftime("%d/%m/%Y")

            if declared_date_str == datetime.today().date().strftime("%d/%m/%Y"):
                dispatcher.utter_message(text="Τα ημερήσια δεδομένα ανακοινώνονται ζωντανό χρόνο, θα ήταν καλύτερο να επισκεφθείς την επίσημη σελίδα του ΕΟΔΥ εδώ https://eody.gov.gr/category/covid-19/")
            else:
                import pandas as pd
                import numpy as np
                try:
                    nyrros = pd.read_excel(NYRROS_FILE_PATH, sheet_name="all")
                except FileNotFoundError:
                    dispatcher.utter_message(text="Δυστυχώς, αντιμετωπίζω πρόβλημα στη σύνδεση με τη βάση δεδομένων. Καλύτερα να με ρωτήσεις αργότερα πάλι ή να ενημερωθείς από την επίσημη σελίδα του ΕΟΔΥ εδώ https://eody.gov.gr/category/covid-19/.")
                    print("Nyrros's file is not on data/external_data_sources. Maybe cronjob isn't working.")
                    me.disconnect("COVID_VA_DB")
                    return [SlotSet("brief_time", None),SlotSet("requested_city", None)]

                nyrros = nyrros.fillna(0)

                result = nyrros.loc[nyrros["Ημερομηνία"] == declared_date]
                
                if result.empty:
                    dispatcher.utter_message("Δεν γνωρίζω για την συγκεκριμένη ημερομηνία δυστυχώς.")
                else:
                    total_cases = result["ΣΥΝΟΛΟ\nCASES"].to_numpy(dtype=np.uint)
                    new_cases = result["Νέα Κρούσματα"].to_numpy(dtype=np.uint)
                    total_deaths = result["DEATHS"].to_numpy(dtype=np.uint)
                    new_deaths = result["new deaths"].to_numpy(dtype=np.uint)

                    dispatcher.utter_message(text="Σύμφωνα με την ημερομηνία που ζήτησες ({}) στην Ελλάδα υπήρξαν {} νέα κρούσματα με συνολικό αριθμό κρουσμάτων {}, {} νέους θανάτους και {} θανάτους συνολικά. ".format(
                                                    declared_date_str, new_cases[0], total_cases[0], new_deaths[0], total_deaths[0]))

        me.disconnect("default")
        return [SlotSet("brief_time", None),
                SlotSet("brief_country", "Ελλάδα"),
                SlotSet("requested_city", None)]


class ActionUtterGreetings(Action):
    ''' Use this to greet the user according to the time of day
    '''
    def name(self):
        return "action_utter_greetings"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # get hour of the day, rounded down (e.g. for 14.38 returnes 14)
        hour=datetime.utcnow().hour+2
        morning_hours=list(range(4,13))
        greeting="Γεια!"
        if hour in morning_hours:
            greeting="Καλημέρα!"
        else:
            greeting="Καλησπέρα!"
        dispatcher.utter_message(text=greeting)
        return []

class ActionUtterGoodbye(Action):
    ''' Use this to farewell the user according to the time of day
    '''
    def name(self):
        return "action_utter_goodbye"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # get hour of the day, rounded down (e.g. for 14.38 returnes 14)
        you_did_not_accept_terms="Οτιδήποτε άλλο εκτός από θετική απάντηση («Ναί») σημαίνει ότι δεν συναινείτε στους όρους χρήσης. Αν δεν συναινείτε στους όρους χρήσης δεν μπορείτε να χρησιμοποιήσετε το chatbot."
        hour=datetime.utcnow().hour + 2
        morning_hours=list(range(4,13))
        afternoon_hours=list(range(13,19))
        farewell="Γεια σας!"
        if hour in morning_hours:
            farewell="Καλή συνέχεια με την υπόλοιπη ημέρα σου!"
        elif hour in afternoon_hours:
            farewell="Καλό απόγευμα!"
        else: 
            farewell="Καληνύχτα!"
        dispatcher.utter_message(text=you_did_not_accept_terms)
        #dispatcher.utter_message(text=farewell)
        return []

class ActionEndConversation(Action):
    ''' Ends the conversation.

    Perform necessary cleanup, or any post-conversation actions here'''

    def name(self):
        return "action_end_conversation"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        end_message="Μην ξεχάσεις να συμπληρώσεις το ερωτηματολόγιο που βρίσκεται στα αριστερά." 
        end_message_cont="[Η Θεανώ μόλις έκλεισε το chat, αν θέλεις να μιλήσεις ξανά μαζί της πρέπει να κλείσεις και να ανοίξεις ξανά την ιστοσελίδα (το refresh δεν αρκεί).]"
        dispatcher.utter_message(text=end_message)
        dispatcher.utter_message(text=end_message_cont)
        logging.info("ending conversation...")

        # using rasa_sdk's conversation paused, might need to 
        # switch to rasa.core.events.ConversationPaused() in the future.
        return [ConversationPaused()]

class vaccination_stats_form_submit(Action):
    def name(self):
        return "vaccine_stats_form_submit"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        import pandas as pd
        from datetime import datetime, timedelta

        total_covid_icus = TOTAL_COVID_ICUS

        try:
            owid_data = pd.read_csv(OWID_FILE_PATH)
        except FileNotFoundError:
            dispatcher.utter_message(text="Δυστυχώς, αντιμετωπίζω πρόβλημα στη σύνδεση με τη βάση δεδομένων. Καλύτερα να με ρωτήσεις αργότερα πάλι ή να ενημερωθείς από την επίσημη σελίδα του ΕΟΔΥ εδώ https://eody.gov.gr/category/covid-19/.")
            print("Owid vaccination data is not on data/external_data_sources. Maybe cronjob isn't working.")

            return [SlotSet("brief_time", None)]

        owid_data = owid_data.fillna(0)

        declared_date = tracker.get_slot("brief_time")

        temp = declared_date.split("T")[0]
        temp = temp.split("-")
        declared_date = datetime(int(temp[0]), int(temp[1]), int(temp[2]))

        declared_date_prior = declared_date - timedelta(days = 1)

        declared_date_str = declared_date.strftime("%d/%m/%Y")

        data_for_date = owid_data.loc[owid_data["date"] == declared_date.strftime("%Y-%m-%d")]

        msg_output = ""

        total_vaccinations = int(owid_data.iloc[[-1]]["total_vaccinations"])

        if data_for_date.empty:
            msg_output += "Δυστυχώς, δεν γνωρίζω για την συγκεκριμένη ημερομηνία."

            if declared_date_str == datetime.today().date().strftime("%d/%m/%Y"):
                msg_output += " Τα δεδομένα γίνονται διαθέσιμα κάποια στιγμή το μεσημέρι."

            last_date_avalaible = owid_data["date"].iloc[-1]
            last_avalaible_data = owid_data.loc[owid_data["date"] == last_date_avalaible]

            temp = last_date_avalaible.split("-")
            declared_date = datetime(int(temp[0]), int(temp[1]), int(temp[2]))

            msg_output += " Συνολικά, σύμφωνα με τα τελευταία διαθέσιμα δεδομένα ({}), έχουν γίνει {} εμβολιασμοί στην Ελλάδα.".format(declared_date.strftime("%d/%m/%Y"), int(last_avalaible_data["total_vaccinations"]))
        
        else:
            data_for_date_prior = owid_data.loc[owid_data["date"] == declared_date_prior.strftime("%Y-%m-%d")]
            
            if data_for_date_prior.empty:
                new_people_vaccinated = int(data_for_date["people_vaccinated"])
                new_people_fully_vaccinated = int(data_for_date["people_fully_vaccinated"])
            else:
                new_people_vaccinated = int(data_for_date["people_vaccinated"]) - int(data_for_date_prior["people_vaccinated"])
                new_people_fully_vaccinated = int(data_for_date["people_fully_vaccinated"]) - int(data_for_date_prior["people_fully_vaccinated"])

            new_people_fully_vaccinated = "κανένας" if not(new_people_fully_vaccinated) else new_people_fully_vaccinated
            
            msg_output += " Στις {}, {} έχουν κάνει την πρώτη δόση του εμβολίου και {} την δεύτερη. Συνολικά, σύμφωνα με τα τελευταία διαθέσιμα δεδομένα, έχουν γίνει {} εμβολιασμοί στην Ελλάδα. ".format(declared_date_str, new_people_vaccinated, new_people_fully_vaccinated, total_vaccinations)
            
        dispatcher.utter_message(text = msg_output)

        return [SlotSet("brief_time", None)]

class icu_stats_form_submit(Action):
    def name(self):
        return "icu_stats_form_submit"

    def run(self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        import pandas as pd
        from datetime import datetime, timedelta

        try:
            nyrros = pd.read_excel(NYRROS_FILE_PATH, sheet_name="ΜΕΘDeaths")
        except FileNotFoundError:
            dispatcher.utter_message(text="Δυστυχώς, αντιμετωπίζω πρόβλημα στη σύνδεση με τη βάση δεδομένων. Καλύτερα να με ρωτήσεις αργότερα πάλι ή να ενημερωθείς από την επίσημη σελίδα του ΕΟΔΥ εδώ https://eody.gov.gr/category/covid-19/.")
            print("Nyrros's file is not on data/external_data_sources. Maybe cronjob isn't working.")

            return [SlotSet("brief_time", None)]

        nyrros.fillna(0)

        declared_date = tracker.get_slot("brief_time")

        temp = declared_date.split("T")[0]
        temp = temp.split("-")
        declared_date = datetime(int(temp[0]), int(temp[1]), int(temp[2]))

        declared_date_str = declared_date.strftime("%d/%m/%Y")

        new_ICUs = nyrros.loc[nyrros["Unnamed: 0"] == declared_date.strftime("%Y/%m/%d")]["ΜΕΘ"]

        msg_output = ""

        if new_ICUs.empty:
            msg_output += "Δυστυχώς, δεν γνωρίζω για την συγκεκριμένη ημερομηνία."
        else:
            msg_output += "Στις {}, ο αριθμός των διασωληνωμένων ήταν {} με αποτέλεσμα το {:.1f}% των ΜΕΘ για τον κορωνοϊό να είναι κατειλημμένες.".format(declared_date_str, int(new_ICUs), (int(new_ICUs) / TOTAL_COVID_ICUS * 100))

        dispatcher.utter_message(text = msg_output)

        return [SlotSet("brief_time", None)]
class ActionSmartSuggestion(Action):

    def name(self) -> Text:
        return "action_smart_suggestion"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        """
        If bot proposes to user and user doesnt aggree, the bot won't ask twice.
        If the user asks about the topic, the bot won't propose that topic.
        """

        mock_sug=input("enter bot suggestion:\n")
        while not mock_sug in functionality_proposal:
            mock_sug=input("enter bot suggestion:\n")

        if mock_sug=="no_action":
            utter_fuse = ["Θα ήθελες να σε βοηθήσω με κάτι άλλο;",
                        "Μπορώ να σε βοηθήσω κάπως αλλιώς;",
                        "Πως αλλιώς θα μπορούσα να σε βοηθήσω;",
                        "Θέλεις να σε βοηθήσω με κάτι άλλο;"]

            dispatcher.utter_message(text = sample(utter_fuse, 1)[0])
        else:
            proposed_functionality = functionality_proposal[mock_sug]
            dispatcher.utter_message(text=proposed_functionality)
        
        return[SlotSet("bot_suggestion", mock_sug)]


        import redis

        try:
            r = redis.Redis(host = REDIS_SERVICE_HOST, port = REDIS_SERVICE_PORT, db = REDIS_DB, decode_responses = True)
            r.get("v")  #redis is lazy. Thus a query is needed to verify the connection.
        except Exception as e:
            print("Redis failed. Maybe its not up.")

            utter_fuse = ["Θα ήθελες να σε βοηθήσω με κάτι άλλο;",
                        "Μπορώ να σε βοηθήσω κάπως αλλιώς;",
                        "Πως αλλιώς θα μπορούσα να σε βοηθήσω;",
                        "Θέλεις να σε βοηθήσω με κάτι άλλο;"]

            dispatcher.utter_message(text = sample(utter_fuse, 1)[0])

            return[SlotSet("bot_suggestion", "no_action")]

        #get all the previous intents because the action is agnostic to other actions if action smart fuse hasnt been called
        #maybe we could set a id_pointer to remember how many actions has been 'scraped' until the current action for each user

        #getting the history of conversation and extracting intents
        msg = tracker.current_state()

        user_events = [x for x in msg["events"] if x["event"] == "user"]
        intent_list = [event["parse_data"]["intent"]["name"] for event in user_events if event["parse_data"]["intent"]["name"] not in [None, "nlu_fallback"]]
        # print(intent_list)
        # print(list_of_functionalities)

        usr_id = tracker.sender_id

        if not(r.exists(usr_id + "_list_functionalities")):
            print("Initializing new user with id: {}.".format(usr_id))
            r.rpush(usr_id + "_list_functionalities", *list_of_functionalities)

        #updating the Redis DB
        for intent in intent_list:
            print(intent)
            if not intent in intent_to_functionality_dict:
                continue
            r.lrem(usr_id + "_list_functionalities", 1, intent_to_functionality_dict[intent])
        
        functionality = "no_action"
        
        if uniform(0,1) > NO_ACTION_PROB:
            if r.exists(usr_id + "_list_functionalities") and r.llen(usr_id + "_list_functionalities") > 1:
                remaining_functionality_list_len = r.llen(usr_id + "_list_functionalities")

                remaining_functionality_list = []

                for i in range(remaining_functionality_list_len):
                    remaining_functionality_list.append(r.lindex(usr_id + "_list_functionalities", i))
                
                #print(remaining_functionality_list)
                functionality = sample(remaining_functionality_list, 1)[0]

                if functionality != "no_action":
                    r.lrem(usr_id + "_list_functionalities", 1, functionality)
        
        proposed_functionality = functionality_proposal[functionality]

        if functionality != "no_action":
            dispatcher.utter_message(text = proposed_functionality)
        else:
            utter_fuse = ["Θα ήθελες να σε βοηθήσω με κάτι άλλο;",
                        "Μπορώ να σε βοηθήσω κάπως αλλιώς;",
                        "Πως αλλιώς θα μπορούσα να σε βοηθήσω;",
                        "Θέλεις να σε βοηθήσω με κάτι άλλο;"]

            dispatcher.utter_message(text = sample(utter_fuse, 1)[0])

        return [SlotSet("bot_suggestion", functionality)]

class ActionClearSuggestion(Action):

    def name(self) -> Text:
        return "action_clear_suggestion"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        return [SlotSet('bot_suggestion',"no_action")]

class action_feedback_suggestion_set(Action):
    def name(self) -> Text:
        return "action_suggest_feedback"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        return [SlotSet("bot_suggestion", "feedback_form")]

class action_set_agreed_to_terms(Action):
    def name(self) -> Text:
        return "action_set_agreed_to_terms"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        return [SlotSet("agreed_to_terms","yes")]

class ActionWelcome(Action):
    def name(self) -> Text:
        return "action_utter_welcome_message"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        hour=datetime.utcnow().hour+2
        morning_hours=list(range(4,13))
        greeting="Γεια!"
        if hour in morning_hours:
            greeting="Καλημέρα!"
        else:
            greeting="Καλησπέρα!"

        greeting+=" Είμαι η σύμβουλός σου για θέματα σχετικά με τον κορωνοϊό. "
        greeting+="Ενδεικτικά, μπορώ να σου δώσω πληροφορίες σχετικά με τα νέα κρούσματα στην Ελλάδα και στον κόσμο, "
        greeting+="να βρω φαρμακεία, καθώς και να απαντήσω στα βασικά ερωτήματα σχετικά με τον κορωνοϊό. Όσο μιλάμε μην ξεχνάς οτι δεν είμαι γιατρός και δεν μπορώ να σου δώσω ιατρικές συμβουλές, καθώς και οτι ακόμα εξελίσσομαι οπότε δεν ξέρω τα πάντα."
        dispatcher.utter_message(text=greeting)
        dontforget="Όταν τελειώσεις συμπλήρωσε το σύντομο ερωτηματολόγιο που βρίσκεται αριστερά, ώστε να ξέρω τι δεν σου άρεσε και την επόμενη φορά που θα μιλήσουμε να έχω βελτιωθεί!"
        dispatcher.utter_message(text=dontforget)
        return []


class ActionSessionStart(Action):
    def name(self) -> Text:
        return "action_session_start"

    async def run(
      self, dispatcher, tracker: Tracker, domain: Dict[Text, Any]
    ) -> List[Dict[Text, Any]]:

        # the session should begin with a `session_started` event
        events = [SessionStarted()]

        # changing value of a slot so as to enable initial rule execution
        if BOT_STATES_TERMS_AT_BEGINNING:
            events.append(SlotSet("agreed_to_terms","no"))
        # an `action_listen` should be added at the end as a user message follows
        events.append(ActionExecuted("action_listen"))

        return events
