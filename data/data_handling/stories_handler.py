#Script for stories
from dissector import story_dissector
import argparse
import os

help_message = """
A Rasa stories yml clustering script.
----------------------------------------------------------------------------
Due to the massive workload from smart_suggestion, it is very difficult to check the stories for inconsistencies.
Also, there are some cases that those will be unnoticed and won't be logged. This situation demanded control. Thus, 
the story handler was born.
----------------------------------------------------------------------------
The script will print the set of unique N story lines before the input action.

E.g. where input = "  - action: action_end_conversation" or input = "action_end_conversation" and N = 4
Story #no
  - active_loop: feedback_form
  - active_loop: null
  - action: utter_thanks_for_the_form
  - action: action_end_conversation

Read the script for more info.
"""

parser = argparse.ArgumentParser(description=help_message)

def cluster_stories(story_action_section_dict):
    """
    Function to handle the story sections and cluster them together.
    The objective of this script is to find the maximum number of story paths that lead to 
    a certain action item. Also, the action items themselves are extracted.
    
    The output will be a list of dicts with the key being the list of actions 
        and the value a list of story_file_name/story_names 
    """

    story_tree_list = []
    story_stripped_list = []

    for key in story_action_section_dict.keys():

        story_tree = story_action_section_dict[key]

        story_stripped = [x.strip() for x in story_tree]

        if not(story_stripped in story_stripped_list):
            story_stripped_list.append(story_stripped)
            story_tree_list.append(story_tree)
        
    return story_tree_list

def story_tree_prettify(story_tree_list):
    if not(story_tree_list):
        print("(!!) Didn't find anything with the input actions.")
    else:
        for story_tree_num, story_tree in enumerate(story_tree_list):
            print("Story #{}".format(story_tree_num+1))

            temp_pretty_story = ""

            for action in story_tree:
                temp_pretty_story = temp_pretty_story + action + "\n"

            temp_pretty_story = temp_pretty_story[:-1]

            print(temp_pretty_story)

parser.add_argument("-sl", "--story-list", metavar="", nargs = "+", default = ["stories_happy.yml"], help = "The story file to be read.")
parser.add_argument("-a", "--all", action = "store_true", default = False, help = "Boolean variable to specify that you want to run the script for all the avalaible stories_(name).yml")
parser.add_argument("-noe", "--number-of-events-lines", type = int, metavar="", default = 3, help = "Number of actions prior to the action check to be read (input event included).")
parser.add_argument("-etcl", "--event-to-check-list", metavar="", nargs = "+", default = ["  - action: action_end_conversation"], help = "List of events to search for.")
parser.add_argument("-pr", "--prettify", action = "store_true", default = False, help = "Flag to prettify the output.")
parser.add_argument("-ap", "--absolute-path", action= "store_true", default = False, help = "Flag to treat paths as absolute")
args = parser.parse_args()

if args.absolute_path:
    story_path = ""
else:
    story_path = "../"

if args.all:
    story_list = []
    #read all the stories_(something).yml file names
    for file_name in os.listdir("../"):
        if file_name.startswith("storie"):
            story_list.append(file_name)
else:
    story_list = args.story_list

output_story_cases = {}

event_lines = args.number_of_events_lines
events_to_check = args.event_to_check_list

for story_file in story_list:
    diss = story_dissector()
    diss.go_to_first_story(story_path + story_file)

    while diss.has_next_story:
        story = diss.get_next_story()

        story_event_lines = story.split("\n")
        
        #len("- story: ") = 9, thus indexing from 9
        story_name = story_event_lines[0][9:]

        story_event_lines = story_event_lines[2:]

        for event_line_no, story_event_line in enumerate(story_event_lines):
            for check_event in events_to_check:
                if check_event in story_event_line:
                    if event_line_no < event_lines:
                        output_story_cases[story_file + "/" + story_name] = story_event_lines[:event_line_no + 1]
                    else:
                        output_story_cases[story_file + "/" + story_name] = story_event_lines[event_line_no - event_lines + 1: event_line_no + 1]

    print("--------------------------------------------")
    print("Common sub-stories for {}".format(story_file))

    if args.prettify:
        story_tree_prettify(cluster_stories(output_story_cases))
        pass
    else:
        print(cluster_stories(output_story_cases))