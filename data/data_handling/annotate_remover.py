# -*- coding: utf-8 -*-
#---------------------
import argparse
import re

help_message = """
A Rasa yml cleanser script. Remove annotations from nlu-like files.
--------------------------------------------------------------------------
Rasa yml scripts data nested in a predefined yml formated file.
The purpose of this script is to extract the data from the yml without the yml format 
and intents.
--------------------------------------------------------------------------
Intents are defined inside square brackets and annotated with parenthesses or curly braces.
"""

parser = argparse.ArgumentParser(description=help_message)

parser.add_argument("-if", "--input-file", metavar="", default = "nlu.yml", help = "File to cleanse.")
parser.add_argument("-of", "--output-file", metavar="", default = "nlu_cleansed.yml", help = "File to save the cleansed one.")
parser.add_argument("-il", "--intent-list", metavar="", nargs = "+", default=["whole"], help = "Boolean for giving an intent list or not")
parser.add_argument("-wi", "--with-intents", action = "store_true", default = False, help = "Boolean for appending each cleansed data intent in output file.")
args = parser.parse_args()

def count_entities(input_string):
    """
    Count how many entities there are in the nlu data
    Return the positions that parentheses were found

    Parentheses might be used also as a part of the nlu data. 
    Thus, the parentheses is eligible if it is preceded by ]

    Args:
        input_string: the input string to be tested
    Returns:
        entity_index: List of integers that denote the index 
                        of the entity in the input string
    """
    
    entity_index = []

    for count,letter in enumerate(input_string):
        if (letter == "(") and (input_string[count-1] == "]"):
            entity_index.append(count)
    
    return entity_index

def find_index_of_intents(nlu_path,intent_list):
    """
    Find in which lines the data of the given intent lays

    Args:
        nlu_path: path to nlu.yml
        intent_list: list of intents to get the data
    Returns:
        row_numbers: list of list of 2 int denoting start and ending line.

    Should an automated intent string checker made?
    """
    
    with open(nlu_path, "r") as nlu_f:

        file_begin = nlu_f.tell()

        row_numbers = []

        for intent_count,intent in enumerate(intent_list):
            """
            Search from the beggining of the file until an intent 
            is found. Gather all the data underneath the found 
            intent until a new intent or synonym tag is found. 
            Start unless there isn't any intents left or only synonyms are found.
            """

            nlu_f.seek(file_begin)

            row_numbers.append([])

            intent_line_width = 0

            found_intent = False
            for line_count,line in enumerate(nlu_f):

                if "intent" in line:
                    if not(found_intent):
                        #len("- intent: ")==10, thus line[9::] is used
                        if intent in line[10::]:
                            intent_width = line_count
                            row_numbers[intent_count].append(line_count)
                            found_intent = True
                    else:
                        intent_line_width = line_count - intent_width
                        row_numbers[intent_count].append(intent_line_width)
                        break
                elif "synonym" in line:
                    intent_width = line_count - intent_width
                    row_numbers[intent_count].append(intent_width)
                    break

    return row_numbers

def parentheses_remover(input_list):
    """
    Removes the parentheses with the entity name from nlu.yml

    When an entity is found, the (name_of_entity) should be removed 
    from every nlu data that was extracted.

    Args:
        input_list: input list of nlu data that were extracted from nlu.yml
    Returns:
        input_list (processed): cleansed list from entities
    """
    for input_count, input_string in enumerate(input_list):
        input_list[input_count] = re.sub(r'\([^)]*\)', '', input_string)
    
    return input_list

def curly_braces_remover(input_list):
    """
    Function to remove curly braces from a list of strings.

    Removing entity annotations. Removing extra spaces introduced by removing curly braces.

    Args:
        input_list: list of phrases (strings) with curly braces.
    Returns:
        input_list (processed): list of cleansed phrases.
    """

    for sentence_no,sentence in enumerate(input_list):
        input_list[sentence_no] = re.sub(r'\{[^}]*\}', '', sentence)

    return input_list


def bracket_remover(input_list):
    """
    Function to remove entity brackets

    Args:
        input_list: list of nlu data
    Returns:
        input_list (processesd): list of un-bracketed list of nlu data
    """

    for count,data in enumerate(input_list):
        input_list[count] = input_list[count].replace("[", "")
        input_list[count] = input_list[count].replace("]", "")

    return input_list

def cleanse_nlu_data(data_path, output_path, intent_list = "whole", with_intents = False):
    """
    Function to produce a new txt with the nlu data alone, without annotations etc

    Disclaimer:
        The nlu file will have to be as is. 
        The program will terminate when it finds Synonyms!
    
    Args:
        data_path: input path for nlu.yml
        output_path: path for the txt to be stored
        intent_list: "whole" for the whole nlu.yml or a list of intent names
        with_intent: If the user wants-needs the intents of each phrase to be present.
            This will export a csv file with phrase,intent_name
    """

    with open(data_path, "r") as nlu_f:
        
        file_begin = nlu_f.tell()

        exclusion_list = ["version","nlu","intent","examples"]

        if intent_list == "whole":

            nlu_data_list = []

            for line in nlu_f:

                found_ex_word = False

                for exclusion_word in exclusion_list:
                    """
                    The longest list member name for Rasa nlu.yml is "  examples".
                    This is 10 char long and thus, the [0:10].
                    """
                    if exclusion_word in line[0:10]:

                        #keep also intent names
                        if exclusion_word == "intent":
                            intent_name = line[10::]
                        
                        found_ex_word = True
                        break
                if found_ex_word:
                    continue
                elif "synonym" in line[0:10]:
                    break
                
                #nlu data set "    - " and \n removal
                msg_to_write = line[6::]
                msg_to_write = msg_to_write[:-1]

                #removing commas
                msg_to_write = msg_to_write.replace('"', '""')
                msg_to_write = '"' + msg_to_write + '"'

                #save intent_name also on the output
                if with_intents:
                    msg_to_write = msg_to_write + "," + intent_name
                    msg_to_write = msg_to_write[:-1]

                nlu_data_list.append(msg_to_write + "\n")
        else:
            intent_beg_and_width = find_index_of_intents("data/nlu.yml",intent_list)

            nlu_data_list = []

            for intent_params in intent_beg_and_width:
                
                nlu_f.seek(file_begin)

                skip_lines_till_intent = intent_params[0]
                intent_width = intent_params[1]

                for i in range(0,skip_lines_till_intent):
                    nlu_f.readline()

                for i in range(0,intent_width):
                    temp_line = nlu_f.readline()

                    found_ex_word = False

                    for exclusion_word in exclusion_list:
                        if exclusion_word in temp_line[0:10]:
                            found_ex_word = True
                            break
                    if not(found_ex_word):
                        nlu_data_list.append(temp_line[6::])

    #strip brackets and intent names
    cleansed_data_list = parentheses_remover(nlu_data_list)

    cleansed_data_list = curly_braces_remover(cleansed_data_list)

    cleansed_data_list = bracket_remover(cleansed_data_list)

    with open(output_path, "w") as data_write_f:
        for data in cleansed_data_list:
            data_write_f.write(data)

    return None

#argument handling
if args.intent_list[0] == "whole":
    intent_list = "whole"
else:
    intent_list = args.with_intents

if args.input_file != "nlu.yml":
    if args.output_file == "nlu_cleansed.yml":
        input_file_name = args.input_file

        output_file_name = input_file_name.split(".yml")[0] + "_cleansed.yml"
else:
    input_file_name = args.input_file
    output_file_name = args.output_file

input_file_name = input_file_name
output_file_name = output_file_name

cleanse_nlu_data(input_file_name, output_file_name, intent_list,args.with_intents)