import io
import sys

def check_has_reserved_word(str_line):
    words_to_check = ["version", "intent", "examples", "synonym", "nlu"]

    for word in words_to_check:
        if word in str_line:
            return True

    return False

for nlu_file in sys.argv[1::]:
    if nlu_file.split(".")[1] != "yml":
        print("Please insert a Rasa nlu yaml file. Aborting...")
        exit()

    new_name = nlu_file.split(".")[0] + "_unduplicated.yml"
    with io.open("../" + new_name, mode = "w", encoding = "utf-8") as nlu_copy:
        with io.open("../" + nlu_file, mode = "r", encoding = "utf-8") as f:
            checked_phrase_dict = {}

            lines = f.readlines()
            
            #search for lines and nlu duplicates
            for row_counter, new_line in enumerate(lines):
                if check_has_reserved_word(new_line):
                    nlu_copy.write(new_line)
                elif new_line.isspace():
                    nlu_copy.write(new_line)
                else:
                    phrase = new_line.split("- ")[1]

                    if not(phrase in checked_phrase_dict.keys()):
                        checked_phrase_dict[phrase] = row_counter
                        nlu_copy.write(new_line)
                    else:
                        print("Duplicate found in lines {} - {}".format(checked_phrase_dict[phrase] + 1, row_counter + 1))