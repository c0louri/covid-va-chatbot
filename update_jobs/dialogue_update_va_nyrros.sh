#!/bin/bash
#
# dialogue_update_va_nyrros.sh
#
# cron job for updating greek data To make this work 
# a. give it executable permissions e.g. 755
# b. add the following line to a file (with permissions 644 root:root) inside /etc/cron.d
# for example
# * 16 * * * <USER> /path/to/dialogue_update_va.sh
# Add only full paths, or it won't work.
# below is a sample file
D=`date`
COVID_REPO_DIR=/home/kpalios/workspace/covid-va/official/covid-va/dialog
# navigate to repo folder
pushd $COVID_REPO_DIR
wget --output-file="log.csv" -O "content.xlsx" "https://docs.google.com/spreadsheets/d/14rKl4TAM05YWj94u3rAkS2PKTSIqYzdCeuXVMtV6ptM/export?format=xlsx&gid=784106715"
cp content.xlsx data/external_data_sources/covid_19gr_nyrros.xlsx
echo "updated Covid VA stats nyrros $D" >> dialogue_va.log
popd

