#!/bin/bash
#
# dialogue_update_owid_va.sh
#
# script for updating owid/covid-19-data repo. To make this work in a regular basis
# a. give it executable permissions e.g. 755
# b. add the following line to a file (with permissions 644 root:root) inside /etc/cron.d
# for example, to run this every day at 4 pm:
# * 16 * * * <USER> /path/to/dialogue_update_owid_va.sh
# Add only full paths, or it won't work.
# below is a sample file
OWID_REPO_DIR=/home/kosmas/workspace/covid-va/official/covid-va/dialog/covid-19-data
COVID_REPO_DIR=/home/kosmas/workspace/covid-va/official/covid-va/dialog
D=`date`
# navigate to covid folder
pushd "$COVID_REPO_DIR"
# 1. pull latest data
if [[ -d "covid-19-data/.git" ]]; then
    # remove folder if exists
    yes | rm -r covid-19-data
fi
# clone it again
git clone --depth 1 https://github.com/owid/covid-19-data.git
# 2. copy into data folder
cp covid-19-data/public/data/vaccinations/country_data/* "$COVID_REPO_DIR/data/external_data_sources/vaccinations/country_data/"


echo "updated OWID data on $D" >> "$COVID_REPO_DIR/dialogue_va.log"
popd
