import re
regex= ""

def remove_extra_spaces(inputstring):
    res = re.sub(r"\s+"," ",inputstring)
    return res

def remove_newline_and_indentation_before_bar(instr):
    res= re.sub(r"\n[ \t]*\|","|",instr)
    return res

with open("regex.txt","r") as f:
#    line = f.readline()
#    while line not in ["_END_","_END_\n"]:
#        print(line)
#        line = line.rstrip()
#        regexes.append(line)
#        line = f.readline()
#
    regex=f.read()
    print(regex)

regex=remove_newline_and_indentation_before_bar(regex)
print(regex)
regex_without_extra_spaces=remove_extra_spaces(regex.strip())
print(regex_without_extra_spaces)

string = input()
print(bool(re.search(regex_without_extra_spaces,string)))
