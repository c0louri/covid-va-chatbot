import re
import sys

if len(sys.argv) < 2:
    print("No file is given for checking")
    exit(2)

yaml_fails_file = sys.argv[1]

failed_stories = 0
with open(yaml_fails_file, 'r') as y_f:
    lines = y_f.readlines()
    for line in lines:
        if line.startswith("- story:"):
            # line is a start of a failed story test
            failed_stories += 1

if failed_stories == 0:
    print("Success! No fail occured!")
    exit(0)
else:
    print("Failures occured in {} stories!".format(failed_stories))
    print("(further info in the failed_test_stories.yml)")
    exit(1)