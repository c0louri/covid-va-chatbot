# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa.shared.core.events import UserUttered, ActionExecuted


class ActionMockUserUtterance(Action):

    def name(self) -> Text:
        return "action_mock_user_utterance"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="I hear a voice... Did you say 'bye'?")
        action_dict = {
                    "event": "action",
                    "name": "action_listen"
                    }
        user_utterance_dict= {
	    "event": "user",
	    "text": "bye",
	    "parse_data": {
		"intent": {
		    "name": "goodbye",
		    "confidence": 0.9
		},
		"entities": []
	    },
	    "metadata": {},
	}
        return [action_dict,user_utterance_dict]
