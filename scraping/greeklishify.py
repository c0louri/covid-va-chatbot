"""
Function to change Greek to Greeklish.
"""

def Remove_exotic_notes(word):
    exotic_note_dict = {
        'ά':'α',
        'έ':'ε',
        'ή':'η',
        'ί':'ι',
        'ό':'ο',
        'ύ':'υ',
        'ώ':'ω',
        #dialitika
        'ϊ':'ι',
        'ΐ':'ι',
        'ϋ':'υ',
        'ΰ':'υ',
        }
    
    
    out = word
    for letter in word:
        if letter in exotic_note_dict:
            out = out.replace(letter, exotic_note_dict[letter])
        
    return out

def Greeklishify(word):
    #dictionary
    greek_to_lish_dict = {
        "α":"a",
        "β":"b",
        "γ":"g",
        "δ":"d",
        "ε":"e",
        "ζ":"z",
        "η":"i",
        "θ":"th",
        "ι":"i",
        "κ":"k",
        "λ":"l",
        "μ":"m",
        "ν":"n",
        "ξ":"ks",
        "ο":"o",
        "π":"p",
        "ρ":"r",
        "σ":"s",
        "τ":"t",
        "υ":"y",
        "φ":"f",
        "χ":"x",
        "ψ":"ps",
        "ω":"o",
        #teliko s
        "ς":"s", 
        #keno -> -
        " ":"-"}
    
    #lowercase the message
    out = word.lower()
    
    #Get rid of the annotations
    out = Remove_exotic_notes(out)
    
    #change each letter with the use of the dictionary
    temp = out
    for letter in temp:
        if letter in greek_to_lish_dict:
            temp = temp.replace(letter, greek_to_lish_dict[letter])
    
    #return the word
    return temp