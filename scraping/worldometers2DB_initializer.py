from covid import Covid
import os
from country_name_greek2english import country_greek2english
from documents import covid_api_data
from datetime import datetime
import mongoengine as me
from country_name_greek2english import country_english2greek

MONGO_SERVICE_HOST = os.environ["MONGO_SERVICE_HOST"] if "MONGO_SERVICE_HOST" in os.environ else "localhost"
MONGO_SERVICE_PORT = int(
    os.environ['MONGO_SERVICE_PORT']) if "MONGO_SERVICE_PORT" in os.environ else 27017

print("Logging to the API.")

#initialize the Covid object
covid = Covid(source = "worldometers")

'''
The data will be saved on the same document inside a dict field.
The outter dict will have a datetime for the date the data were scraped
and inside dict will have all the info that the covid api provides 
(maybe will exclude the longtitude,lattitude,time)

The datetime object will be stringified with a default method.
'''

print("Getting the current data...")
#read all the data for today
all_data_dict = covid.get_data()

print("Connecting to the db")
#me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)

#current date in format day/month/year
str_date = datetime.utcnow().strftime("%d/%m/%Y")

lista = []

#Create the db and passd Greek and Enlgish names
for new_country in all_data_dict[1::]:
    obj = covid_api_data()
    
    english_name = new_country["country"]
    
    #get the greek name
    greek_name = country_english2greek(english_name.lower())
    
    if greek_name != "None":
        obj.Greek = greek_name
        obj.English = english_name
        
        obj.save()
    else:
        print("Found an exception for {}:".format(english_name))
        
'''
#the first dict is for the world without a country name
for new_country in all_data_dict[1::]:
    name = new_country["country"]
    
    #the names of the countries are saved in the d, use .lower() method
    #find if it exists in the DB
    covid_info_obj = covid_api_data.objects(English = name.lower())
    
    #if the db doesnt respond to the query it will return an empty list obj
    if len(covid_info_obj) == 0:
        pass
    else:
        #delete the name as we have it already saved
        new_country.pop("country")
        covid_info_obj[0].covid_api_info_dict.update({str_date : new_country})
        
        covid_info_obj.save()
'''