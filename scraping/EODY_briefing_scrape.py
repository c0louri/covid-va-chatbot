from bs4 import BeautifulSoup as bs
import requests
import mongoengine as me
import os

from documents import *

MONGO_SERVICE_HOST = os.environ["MONGO_SERVICE_HOST"] if "MONGO_SERVICE_HOST" in os.environ else "localhost"
MONGO_SERVICE_PORT = int(
    os.environ['MONGO_SERVICE_PORT']) if "MONGO_SERVICE_PORT" in os.environ else 27017

me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)

#Get all the available objects
EODY_briefing.objects()

#set a valid agent
agent = {"User-Agent":"Mozilla/5.0"}

#order by descending (to get the latest date and update it)
for brief in EODY_briefing.objects().order_by("-date"):
    if brief.has_data:
        print("Already scraped data from date: {}".format(brief.date))
    #check if the brief has been scraped already from the has_data field in EODY_briefing collection
    else:
        #get the url
        url = brief.briefing_url_2scrape
        
        #scrape the paragraph needed
        try:
            source = requests.get(url, headers=agent)
        except requests.exceptions.ConnectionError:
            print("The webpage does not respond.")
            exit()
        
        #extract the new cases,total cases, new deaths, total deaths
        #soupify 
        soup = bs(source.content, 'lxml')
        
        x = soup.find_all("p")
        '''
        New cases and total cases are located in the second paragraph.
        New deaths and total deaths are located in the fifth.
        
        The result from the find_all method is a ResultSet with all the
        paragraph tags in the html code. We stringify them and exploit the 
        html code schema to extract the information needed (thus the split 
        method)
        '''
        try:
            new_cases = str(x[1]).split("<strong>")[1].split("</strong>")[0].replace(" νέα", "")
        except IndexError:
            print("New cases format has changed at {}".format(brief.date))
            continue
        
        try:
            y=int(brief.date.year)
            m=int(brief.date.month)
            d=int(brief.date.day)
            if (y==2020) and ((m==9 and d > 9) or (m==10) or (m==11 and d<4)):
                total_cases = str(x[1]).split("<strong>")[4].split("</strong>")[0]
            else:
                total_cases = str(x[1]).split("<strong>")[3].split("</strong>")[0]
        except IndexError:
            print("Total cases format has changed at {}".format(brief.date))
            continue 
        try:
            new_deaths = str(x[4]).split("<strong>")[1].split("</strong>")[0]
        except IndexError:
            print("New deaths format has changed at {}".format(brief.date))
            continue
        
        #check if there arent any new deaths (it is annotated as "κανένας νέος θάνατος"
        if "κανένας" in new_deaths:
            new_deaths = 0

        try:
            total_deaths = str(x[4]).split("<strong>")[2].split("</strong>")[0]
        except IndexError:
            print("Total deaths format has changed at {}".format(brief.date))
            continue
        
        #Cast them as integers
        new_cases = int(new_cases)
        total_cases = int(total_cases)
        new_deaths = int(new_deaths)
        total_deaths = int(total_deaths)
        
        #save them to the DB using the update method of MongoEngine
        brief.new_cases = new_cases
        brief.total_cases = total_cases
        brief.new_deaths = new_deaths
        brief.total_deaths = total_deaths
        
        #set the has data as true for further uses and refresh
        brief.has_data = True
        
        #save also the whole 1-4 paragraphs without the tags
        #first we have to process it
        #empty string to append the paragraphs without the HTML tags
        para = ""
        
        #we need the 2-5 paragraphs
        for paragraph in x[1:5]:
            temp = str(paragraph)
            
            #remove the tags
            temp = temp.replace("<p>", "")
            temp = temp.replace("</p>", "")
            temp = temp.replace("<strong>", "")
            temp = temp.replace("</strong>", "")
            
            para = para + temp
            
        #save the paragraph in a StringField
        brief.whole_briefing = para
        
        brief.save()
        
