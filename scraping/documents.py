# -*- coding: utf-8 -*-
"""
MongoDB document for with the name of the region/city and the url to use for
the scraping procedure.

"""
import mongoengine as me
import datetime
import os
#Define a document

MONGO_SERVICE_HOST = os.environ["MONGO_SERVICE_HOST"] if "MONGO_SERVICE_HOST" in os.environ else "localhost"
MONGO_SERVICE_PORT = int(
    os.environ['MONGO_SERVICE_PORT']) if "MONGO_SERVICE_PORT" in os.environ else 27017

me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)

'''
City document
Will hold the greek name and its url counterpart to be used for on 
demand scraping.
'''
class City(me.Document):
    #Connection to the mongo, should have a username and password
    me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
    
    #name of the city
    city_name = me.StringField(unique = True)
    #url to scrape
    city_url_2scrape = me.URLField(unique = True)
    
    me.meta = {
        "indexes": ["city_name", "prefecture"]    
    }

'''
Overnight pharmacy document
Will hold the scraped data
The pharmacies are open in 12hour intervals
'''
class Pharma(me.Document):
    #Connection to the DB
    me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
    
    #name of the city
    city_name = me.StringField()
    
    #When the object is formed, the day that the data is scraped will be saved
    last_update = me.DateTimeField(default = datetime.datetime.utcnow)
    
    #List with the scraped names
    name = me.StringField()
    
    #List with the addresses of the open pharmacy
    address = me.StringField()
    
    #List with the number of the open pharmacy
    phone = me.StringField()

'''
General hospital info
'''
class hospital(me.Document):
    me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
    
    name = me.StringField()
    city = me.StringField()
    phone_number = me.StringField()
    website = me.URLField()    
    
'''
Eody url
'''
class EODY_briefing(me.Document):
    me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
    
    has_data = me.BooleanField(default = False)
    
    #url to use for scraping
    briefing_url_2scrape = me.URLField(unique = True)
    
    #date for the url
    date = me.DateTimeField(unique = True)
    
    #the whole briefing
    whole_briefing = me.StringField()
    
    #useful data
    new_cases = me.IntField()
    total_cases = me.IntField()
    new_deaths = me.IntField()
    total_deaths = me.IntField()
    
'''
Country that can be passed in the covid api and greek-english dictionary
'''
class covid_api_data(me.Document):
    me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
    
    Greek = me.StringField()
    English = me.StringField()
    
    covid_api_info_dict = me.DictField()
    
    meta = {
        "collection" : "covid_api_greek2english_country"
    }


#Function to query the name of the city and get the url
def url_fetch(name):
    fetch = City.objects(city_name = name)

    try:
        return fetch[0].city_url_2scrape
    except IndexError:
        print("Η πόλη δεν υπάρχει στην βάση. Δες για ανανέωση ή refresh!")
        
#function to query the url by date from the EODY briefing collection
def eody_url_fetch(day,month,year):
    return EODY_briefing.objects(date = datetime.datetime(year,month,day))
    