'''
Vrisko pharmacy url scraping and saving to mongo
Function for refreshing the cities avalaible from Vrisko
'''

from bs4 import BeautifulSoup as bs
import requests
import os

#Document module
from documents import *
from greeklishify import *

MONGO_SERVICE_HOST = os.environ["MONGO_SERVICE_HOST"] if "MONGO_SERVICE_HOST" in os.environ else "localhost"
MONGO_SERVICE_PORT = int(
    os.environ['MONGO_SERVICE_PORT']) if "MONGO_SERVICE_PORT" in os.environ else 27017

#module for scraping
def extract_source(url):
     agent = {"User-Agent":"Mozilla/5.0"}
     source = requests.get(url, headers=agent).text
     return source

prefecture = []
cities = []
            
def mun_href_name_extraction(scraped_a):
    
    #deleting the <a tag and href>
    temp = scraped_a.split('<a href="')[1]
    
    #spliting name and "> 
    temp = temp.split('">')
    
    #temp now has a pure link and the name with a postfix </a>
    href = temp[0]
    name = temp[1].split('</a>')[0]
    
    return name, href


print("Starting the refresh procedure for the cities")

#Set the default page and request the html code
source = extract_source('https://www.vrisko.gr/efimeries-farmakeion/')

#Soupify 
soup = bs(source, 'lxml')

#Getting the municipality data
x = soup.find_all('div', class_ = 'blockContentPrefecture')

#Extract the href and name of the municipalities
x = x[0].find_all('a')

for prefec in x:
    name,href = mun_href_name_extraction(str(prefec))
    
    prefecture.append({'name': name, 'href': href})

#For every prefecture extracted get the cities
for prefec in prefecture:
    source = extract_source(prefec['href'])
    
    soup = bs(source, 'lxml')
    
    x = soup.find_all('td', class_ = "RegionDescription")
    
    if x == []:
        temp = str(soup.find_all(class_ = "Useful_SiteMapTitle")[3])
        
        temp = temp.split('>')[1].split('<')[0]
        
        cities.append(temp)
        
    for city in x:
        temp = str(city)

        temp = temp.split('>')[2].split('</a')[0]

        cities.append(temp)
        

#vriskoobj.cities will have the names of the cities in Greek
prefix_url = "https://www.vrisko.gr/efimeries-farmakeion/"

print("Saving them to the DB")

for city in cities:
    #Initialize the object to be saved in the DB
    city_obj = City()
    
    city_obj.city_name = city
    
    #Greeklishify the name and append it at the prefix 
    greeklished_name = Greeklishify(city)
    
    #form the complete search url
    url = prefix_url + greeklished_name
    
    #pass them to the object
    city_obj.city_url_2scrape = url
    
    #pass it to the DB
    city_obj.save()
    
#Close the DB connection
me.disconnect()