#ReadMe markdown for the explanation of scraping folder
Documents has all the info for the schema's in the DB using mongoengine

EODY_briefing_url_refresh is a function to fetch new url from the official EODY webpage:

```
https://eody.gov.gr/category/anakoinoseis/
```

eody_briefing_url_refresh is a function to scrape and update the DB with the new cases, 
total_cases, new deaths and total deaths from the report that is provided by EODY

vrisko_ph_city_refresh is a function to refresh the avalaible cities and their links

vrisko_ph_scrape is the function to scrape the data from the pharmacy bot (currently its
functionality is placed inside the action in the rasa with the form Pharmacy_request_form)


# Update 11/5 - fixing "wrong new cases reported for Greece from 13-9 to 3-11"

[This commit](4c26187c430d6ad7bffa91e54931bee11266277b) set a condition in scraping: for dates between 13-9 and 3-11 the fourth number in 
the report is stored as total_cases.

To make the VA work one has to remove the mongo documents in `e_o_d_y_briefing` corresponding to the aforementioned dates and 
redo the scraping.


