from bs4 import BeautifulSoup as bs
import requests
import datetime
import mongoengine as me
import os

from documents import *

MONGO_SERVICE_HOST = os.environ["MONGO_SERVICE_HOST"] if "MONGO_SERVICE_HOST" in os.environ else "localhost"
MONGO_SERVICE_PORT = int(
    os.environ['MONGO_SERVICE_PORT']) if "MONGO_SERVICE_PORT" in os.environ else 27017

#connect to the db
me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)

url = "https://eody.gov.gr/category/anakoinoseis/"
agent = {"User-Agent":"Mozilla/5.0"}

try:
    source = requests.get(url, headers=agent)
    
except requests.exceptions.ConnectionError:
    print("The webpage does not respond.")
    exit()

soup = bs(source.content, 'lxml')

#get all the links from the page
yo = soup.find_all("a")

briefing = []
for alpha in soup.find_all("a"):
    if ("επιτήρησης COVID-19" in str(alpha)):
        briefing.append(alpha)

#scrape the urls from the briefings
for brief in briefing:
    briefing_obj = EODY_briefing()
    
    #scrape the url
    briefing_obj.briefing_url_2scrape = brief.get("href")
    
    #scrape the date
    date = brief.get("aria-label").split("(")[-1].split(")")[0]
    date = date.split("/")
    
    print(date)
    
    #Search the last data in the DB and get the date
    
    briefing_obj.date = datetime.datetime(int(date[-1]), int(date[-2]), int(date[-3]))
    try:
        briefing_obj.save()
    except me.errors.NotUniqueError:
        print("The briefing from this date is already in the DB.")
        continue
    
    print("Briefing url is being saved...")
        
print("Finished the refresh! Exitting...")
me.disconnect("COVID_VA_DB")