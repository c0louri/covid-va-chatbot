# -*- coding: utf-8 -*-
"""
Url to Scrape from Vrisko
"""

from bs4 import BeautifulSoup
import requests
import os

from documents import *
import mongoengine as me

MONGO_SERVICE_HOST = os.environ["MONGO_SERVICE_HOST"] if "MONGO_SERVICE_HOST" in os.environ else "localhost"
MONGO_SERVICE_PORT = int(
    os.environ['MONGO_SERVICE_PORT']) if "MONGO_SERVICE_PORT" in os.environ else 27017

def search_exist(name):
    #connect to the DB
    me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
    
    #init the object
    fetch = Pharma.objects(city_name = name)
    
    if len(fetch) == 0:
        return False
    else:
        return True
    
    me.disconnect("COVID_VA_DB")

def scrape_ph_city(name):
    #Connect to the DB
    #Remember that the DB must be up from another terminal!
    me.connect("COVID_VA_DB", host = MONGO_SERVICE_HOST, port = MONGO_SERVICE_PORT)
    
    #Query the given name and fetch the url
    url = url_fetch(name)
    
    if (url == None):
        print("Η πόλη δεν είναι διαθέσιμη!")
        return None
    
    if search_exist(name):
        #The city was found in the DB
        print("I wont scrape")
        return 0
    
    #pass a header to the request (form JSON str)
    agent = {"User-Agent":"Mozilla/5.0"}
    
    #Get request to Vrisko server and return a string var
    source = requests.get(url, headers=agent).text
    
    #soup-ify the str var
    soup = BeautifulSoup(source, 'lxml')
    
    #Exploit the advantage tha all the resuls are in a <a> tag with class 'ResultName'
    #and all addreses are in a <div> with class 'ResultAddr'
    #God bless the dev team of vrisko
    
    names = []
    addresses = []
    phones = []
    
    #name scrape
    for pharma_name in soup.find_all("a", class_ = "ResultName"):
        names.append(pharma_name.text[1::])
    
    #Address scrape
    for address in soup.find_all("div", class_ = "ResultAddr"):
        temp_addr = address.text.replace('\n', '')
        temp_addr = temp_addr.replace('  ','')
        
        addresses.append(temp_addr)
    
    #Phone scrape
    for phone in soup.find_all("span", class_="spPhone"):
        phones.append(str(phone).split(">")[1].split("</")[0])
        
    print("Βρέθηκαν {} φαρμακεία!".format(len(names)))
    
    '''
    #Save them to the DB
    for ph_num in range(len(names)):
        #initialize the object for mongoengine
        ph = Pharma()
        
        ph.city_name = name
        ph.name = names[ph_num]
        ph.address = addresses[ph_num]
        ph.phone = phones[ph_num]
        
        ph.save()
    
    me.disconnect("COVID_VA_DB")
    '''