class story_dissector():
    def __init__(self):
        self.name_of_file = ""
        self.file = ""
        self.new_story_pointer = ""
        self.has_next_story = True

    def collapse_enter(self,story):
        found_letter = False

        while(not(found_letter)):
            if story[-1] == "\n":
                story = story[:-1]
            else:
                found_letter = True

        return story

    def go_to_first_story(self, file_name):
        self.name_of_file = file_name

        self.file = open(self.name_of_file, "r")

        self.file.readline()
        self.new_story_pointer = self.file.tell()

    def get_next_story(self):
        story_txt = ""
        story_beggining_found = True

        self.file.seek(self.new_story_pointer)

        while True:
            
            line = self.file.readline()

            if "- story" == line[0:7]:
                story_txt += line

                while True:
                    self.new_story_pointer = self.file.tell()
                    line = self.file.readline()

                    if not line:
                        self.has_next_story = False
                        break
                    
                    if not("- story" == line[0:7]):
                        story_txt += line
                    else:
                        break
                
                story_txt = self.collapse_enter(story_txt)
                return story_txt

            
