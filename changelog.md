Θεανώ (Theano) is a Voice Assistant configured and engineered by the Institute for Language and Speech Processing of Athena Research Center.
This voice assistant will provide the users with the latest and most accurate information about the novel coronavirus (SARS-CoV2) in Greek language.

<h2>v1.0.0 - Initial release</h2>

<h3>Supported functionalities</h3>

- Scraping, saving and processing data for coronavirus' cases and relevant statistical measurements. The information is based on the official data provided
by the European Center for Disease Prevention (ECDC), greek National Public Health Organization (EODY) and the live data provided by the 
John Hopkins University and worldometers.
- A continuously updated database of the most common questions about covid and the pandemic.
For the time being we have used the FAQ from EODY and in the next update we will append the FAQs from ECDC, CDC and the WHO.
- Live information about operating hours and addresses of pharmacies accross Greece.
- A mini questionaire - diagnosis with the 6 most common symptoms.

<h2>v1.0.1</h2>

<h3>Improvements</h3>

- Updated the FAQ that the bot can handle with questions from the aforementioned sources.
- Trying to solve the problem of non connectivity of the functionalities. Implemented a dialogue variant of a user asking for a Pharmacy. The bot may say that the user should't visit a pharmacy if he/she feels ill.

<h2>v1.0.2</h2>

<h3>Migrated to Rasa 2.0</h3>

- Changed markdown format files to yaml.
- Removed deprecated policies and replaced them with Rules using Rule Policy (Mapping Policy, Fallback Policy, Form Policy).
- Spitted Forms to domain (required slots, slot mapping) and actions (validation, submit).
- Added rules to counterbalance TED policy's shortcomings.
- Changed prefix respond with utter for Response Selector utterances.
- Changed unfeaturized type of slots to any.
- Used Rasa 2.0 modularity.
- Spitted stories to stories_happy, stories_unhappy, stories_interactive.
- Changed DucklingHttpExtractor to DucklingEntityExtractor.
- Moved mongo and Duckling services to cloud.

<h3>Improvements</h3>

- Used Nyrros’s excel for questions about Greek Covid data.
- Added a feedback form to get a grasp of user preferences and comments.
- Changed every utterance to match with the proposed persona of Theano.

<h3>New Features</h3>

- Added a custom component for Greek spell checking. A greeklish to greek and lemmatizer is underway.
- Added a smart action_greetings and goodbye to greet by saying "Καλημέρα" or "Καλησπέρα" with respect to the time of bot-user interaction.

**Problems with TED classifier**

- Through automated migration, TED's performance was very low (~60%).
- Changes on the policies parameters resulted in ~91% accuracy 
    - Changed `max_history` from 5 to 10 
    - Changed default batch size and increment from [64,256] to [4,8] 
    - Changed `augmentation_factor` to 5 
    - Added `dense_dimension` to 64
- Training time for TED has increased fivefold.
- Training for different test users resulted in different fail cases. Even training more than one time in the same environment had the same outcome. TEDs accuracy is almost stochastic.
- TED produces randomly weird failures over the tests. As stated before, each training results in a different failute over tests. The system follows the desired path, until, at some point it predicts something very unnatural. For example, `action_listen`, `feedback_form`, `ill_questionnaire_form` or even nlu fallback were predicted. The last failure is the weirdest because it isn't present in any story or nlu data. This bug is not easy to reproduce, as we have not seen a pattern in these failures. This is considered a WIP, and will be improved in future versions. 
- TED architecture has changed, thus further investigation is obligatory.

<h3>Bugfixes</h3>

- Response Selectors initiated a bot response when the feedback form was active. This wasn't suppose to happen because the slot mapping was set to text. Solved it with wait_for_user_input: false in rules.yml for Response Selectors (https://gitlab.com/ilsp-spmd-all/dialogue/covid-va-chatbot/-/issues/151)
- Feedback form could be initiated more than once. Fixed it with stories and by pausing the conversation when the feedback form ends (solves [this](https://gitlab.com/ilsp-spmd-all/dialogue/covid-va-chatbot/-/merge_requests/53#note_464094316), also provides helps dealing with https://gitlab.com/ilsp-spmd-all/dialogue/covid-va-chatbot/-/issues/153)

<h3>Warnings</h3>

- Nlu fallback is neither present on domain.yml nor on nlu. When the training begins, an warning is logged. It is also an issue in the Rasa forum (https://forum.rasa.com/t/userwarning-intent-nlu-fallback-has-only-1-training-examples-minimum-is-2-training-may-fail/36929/13)

<h2>v1.0.3</h2>

<h3>New features</h3>

- Upgraded to Rasa 2.3. A new feature about incremental learning is avalaible and it will be of great importance to leverage in an on-line fashion the data from users in the new release.

- Covid statistics form update:
    1) Bot can answer about total-worldwide cases

- Vaccination updates in Greece for specific date how many people had their first jab, how many their second and the total number of vaccinations until the last valid update

- How many intensive care units (ICU) are available for a specific date, as well as the capacity percentage for ICUs in Greece

- EODY_faq update:
    1) What is Corona virus
    2) Delete vaccine because the vaccines are avalaible and we don't know enough information about them
    3) Which are the high-risk groups for covid
    4) Which are the long term facilities
    5) EODY phone number for covid-19 information
    6) EODY phone number for psychological support
    7) How long can covid survive on surfaces
    8) Can my pet transmit coronovirus
    9) When will the pandemic end?
    10) How is the term asymptomatic defined?
    11) Was coronavirus made in a lab?
    12) What's the etymology of the word coronavirus?
    13) When and where was coronavirus first identified?
    14) Are smokers at higher risk?
    15) Where can I get valid information about covid?
    16) What is 13033?
    17) What can I do during quarantine?
    18) Where can I get tested for covid?
    19) Which is the best antiseptic gel to use and how can I use it properly? 

- ChitChat faq update:
    1) What is EMY as a continuation of what's the weather outside
    2) What can I ask for
    3) How old are you?
    4) Where do you spend your vacation time at?
    5) What's your favourite genre of music?
    6) What's your favourite colour?
    7) Which languages can you speak?
    8) Where are you located?
    9) How do you spend your spare time

- Appended some General intents:
    1) Ways of protection info and further information about governmental and/or personal (Mixed Initiative)
    2) Underlying conditions and which are the most important (Mixed Initiative)
    3) Test cost and further information about PCR and Rapid (Mixed Initiative)
    4) Test types and which one is the most trustworthy
    5) What information Theano can provide about tests
    6) Positive feedback intent to catch positive phrases either for a good response/information or for Theano's behaviour in general

- Mask faq update:
    1) Explanation about why you shouldn't touch the front of your mask as a continuation for mask proper wearing
    2) Can I wear a mask when I exercise?

- Out of scope update:
    Out of scope information was splitted in several new groups to avoid missclassification errors. Due to the rapid changes about governmental decisions and/or information about covid, it is best to redirect the user in an officially updated site such as the site of EODY. 

    The new responses are the following:

    1) General unknown information
    2) Info about Hospitals
    3) Info about Vaccines
    4) Info about the current governmental decisions
        - When will the lockdown end?
        - When will schools and/or stores open?
    5) The total covid death ration - Is it probable for humanity to end?
    6) Can I use this drug?
    7) Which is the best way to disinfect my home?
    8) Does quarantine have an impact on covid transmission and cases?
    9) Does weather have an impact on covid transmission and cases?

**action smart fuse:**

- Added a new functionality to change the persistent question after every dialogue turn, "How can I help you?", with Theano, proposing a topic that haven't been discussed so far with the user. The bot will propose a new topic with a fixed probability (60-70%). This needed global variable assignment and processing. Redis is used for this reason.

- Added a new script for synthetic story creation. 

- A/B testing will be held to choose the probability that Theano will propose a new topic for discussion.


## 1.0.4 4/6/2021

* Improved data from users conversations from May 2021
* [INTERNAL] Changed smart_fuse name 

## 1.0.5 2/7/2021

* Improved story data from users conversations from May, June 2021 (!135, !140)
* added response to insults (!115)
* [INTERNAL] Refactored action smart suggestion (!138)
* [INTERNAL] Moved rasa x server to a better machine
