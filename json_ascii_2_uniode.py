# display_json_in_unicode.py
import json
import sys
INPUT_F= sys.argv[1] if len(sys.argv) > 1  else "input.json"
OUTPUT_F= sys.argv[2] if len(sys.argv)> 2 else "output.json"
if INPUT_F!="input.json" and OUTPUT_F=="output.json":
    OUTPUT_F=INPUT_F

with open(INPUT_F,"r") as f:
  dic= json.load(f)

with open(OUTPUT_F,"w") as ww:
  json.dump(dic,ww,ensure_ascii=False,indent=4)

