<div align="center">
  <img src="theano_big_plain2.png" width="400" height="400">
  <h1 align="center">Theano: A Greek-speaking conversational agent for COVID-19</h1>
</div>


**_Table of contents:_**

1. [Installation and usage](#installing-dependencies)

* [Quick install](#quick-install)
* [Advanced Install](#advanced-install)

    * [Virtual environment creation](#virtual-environment)
        * [Mac - Linux users](#for-mac-and-linux-users)
        * [Windows users](#for-windows-users)


* [Building from source](#building-from-source)
    
    * [Building Rasa from source](#building-rasa-from-source)
    * [Building Duckling from source](#building-duckling-from-source)


2. [Implementations - Contributions](#implementations---contributions)
 
* [Smart suggestion update](#smart-suggestion-update-procedure-for-adding-new-functionalities)
* [Adding a new action](#how-to-add-a-new-action)
* [Adding a new intent](#how-to-add-a-new-intent)
* [Adding a response on Response Selector](#how-to-add-a-new-response-on-response-selector)
* [Adding a new Response Selector](#how-to-add-a-response-selector)


3. [Miscellaneous](#miscellaneous)

* [How to use a smart suggestion for the first time](#smart-suggestions-first-time)


4. [Evaluation and Testing](#evaluation-procedure---Testing)

* [Testing with Rasa tester and e2e tester](#testing-procedure-after-every-change)
* [Testing core and nlu seperately](#testing-the-core-and-nlu-model-seperately)
* [N-fold Cross-validation](#cross-validation)


5. [Utils: various tools](#utils-various-tools-to-make-our-lives-a-bit-easier)


6. [External micro-Services](#external-services-and-their-respective-place)

7. [Pushing to production](#pushing-to-production)
 * [How to test the deployed chatbot](#how-to-test-the-deployed-chatbot)


8. [RasaX](#rasax)


9. [Troubleshooting](#troubleshooting)

---
# Installing dependencies

## Quick Install

To install Rasa quickly you need:

* Python (version 3.7.3)
    * Download the version of Python that you want for your Operating System from [the official website](https://www.python.org/downloads/release/python-373/)

    **Make sure that the checkboxes for the environment variables is checked (Windows: Add to PATH, MacOS: Add to Environment Variables (.bash_profile))**

*Disclaimer:* MacOS and some Linux distributions are based on Python 2.7 that will be preinstalled with the OS. Thus, the commands that have **pip** and **python** must be changed to **pip3** and **python3** respectivelly.

* updated pip
    
    * after installing python open a terminal (cmd) and run
    ```
    python(3) -m pip install --upgrade pip
    ```
* create a virtual environment. See [Advanced Install section](#advanced-install).

Install rasa
```
pip(3) install rasa
```

Test that rasa is installed correctly by creating a folder with ```mkdir (MKDIR) {folder_path}``` and ```cd {folder_path}```. Both these actions can be done manually with the Graphical User Interface provided by Linux, Windows or MacOS. Then

```
rasa init
```

Wait for the initial project to finish by pressing Y until finished and talk to the bot with:

```
rasa shell
```
---

## Advanced Install

Before pip installing Rasa, make a virtual environment with the pre-installed Python module ```venv```. 

You can also use Anaconda.

The installation of Anaconda is out of this scope, go to https://docs.anaconda.com/anaconda/install/ for the valid instructions.

## Virtual Environment creation

### For Mac and Linux users

Create a new virtual environment run with the following

```
python(3) -m venv {path_for_venv}
```

where (3) is ommited sometimes (for Windows-Mac users), -m is a flag to run a Python module and {path_for_venv} is the path (e.g. \Desktop\Job\Theano) for the environment to be saved.

After that, you need to activate it by

```
source {path_to_venv}\bin\activate
```

or if you are a Visual Studio Code (VSCode) user, press CTRL+Shift+P and find bin\python3.#, where # is the number for your python version.

### For Windows users

Install virtualenv

```
pip install virtualenv
```

Navigate to your project folder (directory)

Create the environment

```
virtualenv {name_of_the_environment}
```

A new folder with the name of the environment will be created. Then, you have to activate that environment by

```
\{name_of_the_environment}\Scripts\activate.bat
```

Proceed with installing [Rasa](#quick-install)

---
## Building from source

### Building Rasa from Source

1) Choose or create a venv and activate it
2) Git clone https://github.com/RasaHQ/rasa.git

```
git clone https://{repository url}
```

3) Cd to the rasa folder you've just downloaded
4) Git checkout to whichever branch you want
5) Upgrade your pip

```
    python(3) -m pip install --upgrade pip
```

6) Install poetry

```
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python - 
```
7) If you want to uninstall it

```
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python - --uninstall
```
8) You need Poetry's bin directory ($HOME/.poetry/bin) in your `PATH` environment variable. If you restart, you don't need to run the next command

```
    source $HOME/.poetry/env
```

9) Set poetry to look to your virtual env

```
    poetry env use (path to virtual env python3.7 or python or python3)
```

10) Install Rasa

``` 
    poetry install
```

11) Be sure to check if Rasa is installed properly by:

```
    rasa --version
```

If it prints version info for Rasa, the installation is completed. If not, you should be sure that your virtual env is activated with 

```
    poetry env info
    poetry env info --path
```
---

### Building Duckling from source

1) Install haskell (programming language). 

```
https://www.fpcomplete.com/haskell/get-started/
```

2) Install pcre and/or pcre-dev

```
sudo apt-get install libpcre3 (and/or libpcre3-dev)
```

3) Clone the Duckling repo

```
git clone https://github.com/facebook/duckling
```

4) Cd inside repos' root folder

5) Run the following for building of Duckling

```
stack build
```

6) Run Duckling

```
stack exec duckling-example-exe
```

7) Use curl (or other request software) to send requests to duckling service

```
curl -XPOST http://0.0.0.0:8000/parse --data 'locale=el_GR&text=Η πρώτη επιλογή'
```
---

# Implementations - Contributions

## Smart suggestion update procedure for adding new functionalities
 
In order to add functionalities for smart suggestion you need to:

1) Update the smart_suggestion_story_snippets.json
    - Each functionality must be mapped to all the possible dialogue outcomes.
2) Update actions intent_to_functionality_dict, list_of_functionalities, functionality_proposal lists and dictionaries.
    - intent_to_functionality_dict is a dict that maps each intent to a given functionality or a no_action
    - list_of_functionalities is a list of all the avalaible Theano functionalities
    - functionality_proposal is a dictionary that maps the chosen functionality name to a string for Theano to utter to the user.
3) Update data/rules.yml. 4 Rules must be added. Their names (after rule: ) are:
    - Feedbackform_activation (name_of_new_functionality)
    - Feedback_denied (name_of_new_functionality)
    - Perform suggested action (name_of_new_functionality)
    - Negative after smart suggestion (name_of_new_functionality)
---

## How to add a new action

In order to add a new action, you must:
1) Inside actions.py, create a new action class that will inherit from Action class
```
class ExampleActionObjectName(Action):
    def name(self) -> Text:
        return "action_example_action"

    async def run(
      self, dispatcher, tracker: Tracker, domain: Dict[Text, Any]
    ) -> List[Dict[Text, Any]]:

        #Place your logic here

        return [EVENTS]
```
2) Overwrite Action object method for name and run. 
2) Append the name of the function (the one in " in run method, not the name of the class you've made) in the actions section. In our example, you should place ```action_example_action```
3) Test that it doesn't have any syntax error by starting the rasa action server service with 

```
rasa run actions
```

4) Test it with the system by talking to the bot with ```rasa shell``` and ```rasa run actions```

5) Perform the respective [testing-validation](#evaluation-procedure---testing) needed to proceed with a merge request 
---

## How to add a new intent

1) Open data/nlu.yml
2) Add a new intent and some examples

```
- intent: example_intent_name
  examples: |
    - example 1
    - example 2
    - example 3
```

3) Add the new intent in the ```intents:``` section in domain.yml

```
intents:
- example_intent_name
```

4) Add a response if the intent doesn't initiate an action in domain.yml responses section
```
responses:
  utter_example_intent_name:
  - text: example response 1
  - text: example response 2
```

5) Add some simple happy/unhappy stories with the intent-response (or action) you have added
6) Add interactive stories with the following command and save them in the data/stories_interactive without adding the domain and nlu elements
```
rasa interactive -m models
```
7) Add test stories in tests/test_conversation_without_feedbackform 
8) Train and test the system as described in [testing section](#evaluation-procedure---testing)

---

## How to add a new response on Response Selector

We have grouped together some intents that have the same context (EODY_faq, questions about masks, Out of scope phrases).
Thus, if you want to add new data for an intent that complies with our Response Selector:
1) Open data/nlu.yml
2) Add your intent

```
- intent: response_selector_name/new_intent_name
  examples: |
    - example 1
    - example 2
```

3) Open domain.yml and add some responses on the ```responses:``` section:

```
responses:
    utter_response_selector_name/new_intent_name
    - text: Response 1
    - text: Response 2
```

4) Test the new Response by following the [testing section](#evaluation-procedure---testing)
---

## How to add a response selector

If some intents can be grouped together and the bot will handle them with a response only, they will have to be grouped together as a response.

1) Domain: add the response selector in the intents section 
```
- response_selector_name:
    is_retrieval_intent: true
```
2) Config: add the response selector and define the training epochs (normally we choose around 50, but it depends on the case)
```
- name: ResponseSelector
  retrieval_intent: out_of_scope
  epochs: 50
```
3) NLU: write the name of the response selector and then the specific intent in order to add examples
4) Responses & Domain: for each response selector intent you have to write a response both in responses and domain
5) Stories: write stories that contain only the response selector name, without the specific intent inside the selector. This means that after adding a core amount of stories for a response selector you do not need to add stories every time you add a functionality
6) With the purpose of checking the test conversations we have added and then correct all failed ones:
```
rasa test -s tests/test_conversation_without_feedbackform.yml  
```
7) To check how the training stories work:
```
rasa test core
```

8) Test the new Response Selector as described in [testing section](#evaluation-procedure---testing)

# Miscellaneous


## Smart Suggestions: first time

### Step 0: prepare existing stories

Add the action that makes the suggestion, `action_smart_suggestion`, to your existing stories, followed by slot `bot_suggestion` being set to 
`no_action`. In other words, add the 3 lines that are displayed below in every point in a story where a suggestion _is likely to_ be made.
```
  - action: action_smart_suggestion
  - slot_was_set:
    - bot_suggestion: no_action
```

### Step 1: generate extra stories

```
python smart_suggestion_story_expander2.py data/stories_happy.yml
python smart_suggestion_story_expander2.py data/stories_unhappy.yml
python smart_suggestion_story_expander2.py data/stories_interactive.yml
python smart_suggestion_story_expander2.py data/stories_interactive_n.yml
```
### Step 2: train the model 

Set the `augmentation factor` to 3 from the config and run
```
rasa train --debug
```

### Step 3: Test it

```
rasa run actions --debug
```
```
rasa shell --debug
```

### Step 4: report failing cases

...or create (interactive) stories to reinforce the TED model.

---

# Evaluation procedure - Testing

## Testing procedure after every change

Each new change or functionality can introduce bugs and/or "break" older functionalities. Thus, it is of great importance to guarantee that the proposed new change is tested thoroughly. We have implemented our custom end-to-end (e2e) tester as well as use the built in Rasa tester. 

After the automated testing procedure, the proposed change must be tested on a macro level by the developer. 

Also, a cross-validation test must be initiated. The commands will be presented in the third subsection of the given section.

### 1) Test with the built in rasa tester (IT DOESNT CHECK ACTION FUNCTIONALITY)
---

```
rasa test
```

or if you want to give a specific test file:

```
rasa test -s tests/{test name}
```

Some bugs were persistent and resulted in the creation of two test files, one that has the feedbackform and one that does not (test_conversation.yml - test_conversation_without_feedbackform.yml). Every feauture has to be tested with the test_conversation_without_feedbackform.yml file and 66/67 tests passed. 

### 2) Test the proper action functionality with our tester:
---

```
cd rasa_e2e_test
python rasa_e2e_test.py {name of test} 
```

or to test the system for every test

```
cd rasa_e2e_test
python rasa_e2e_test.py *.txt
```

We define as a good enough testing when the accuracy is 95% and more in action level testing.


### 3) Testing the core and nlu model seperately
---
When the model is trained, to test it over our dataset run:

- For core testing (Dialogue Management)

```
rasa test core
```

- For nlu testing (Natural Language Understanding, intent-entity extraction)

```
rasa test nlu
```

## Cross validation

---

Each machine learning model is trained with a specific dataset. In order to nullify the problem of overfitting, the model is trained and tested with different-complementary subset of the dataset. A random train/test split can introduce a test set that won't represent the whole spectrum of the phenomena to be modeled and thus, the results won't be accurate. In order to mitigate the aforementioned phenomenon, the experiment is repeated N times (N-fold cross-validation).

```
rasa test --cross-validation
```

For more info checkout the [rasa testing documentation](https://rasa.com/docs/rasa/testing-your-assistant/).

---

# Utils: various tools to make our lives a bit easier :)
1) ### rasa_tiny_client
    A small client as an interconnection to rasa NLU server (rasa run). 
    To use it open 3 terminals where you'll run ```rasa run```, ```rasa run actions```, ```python rasa_tiny_client.py```
    Another way to talk to the bot is to use directly ```rasa shell``` and ```rasa run actions``` which is an integrated way to start the NLU server and the interconnection simultaneously.
2) json_ascii_2_uniode

    This script translates json files with ascii representations of unicode into actual unicode. 
    Usage is `python3 json_ascii_2_unicode.py <input_file> <output_file>`. But has to be executed from the directory in which
    the script is located, i.e. in the root directory of our repository. for example:
    ```
    cd /path/to/covid-va-chatbot
    python3 json_ascii_2_unicode.py gibberish.json output.json

    ```

    or if you want to change the input file itself, just omit the output file
    ```
    cd /path/to/covid-va-chatbot
    python3 json_ascii_2_unicode.py gibberish.json 

    ```
3) smart_suggestion_story_expander

    A script to 'augment' our default stories (stories_happy.yml, stories_unhappy.yml, stories_interactive{_,n,k}.yml, stories_interactive_{_,n,k}_with_smart_suggestion.yml}
    This makes use of dissector.py, which is an object with scripts for fetching the next story of a particular file.
    This script will load the default stories, disect them and place some story snippets from smart_suggestion_story_snippets.json.

    To augment your stories run:

    ```
    python smart_suggestion_story_expander.py 
    ```

4) update_mongo_covid_stats_from_owid_latest

    This script uses the data from `owid/covid-19`, the github repo with covid-19 data from all over the world,
    to update our mongo db. It is run once for each day, using the file of that day, as follows:

    ```
    python3 update_mongo_covid_stats_from_owid_latest.py <VERSION OF FILE public/data/latest/owid-covid-latest.csv> "dd/mm/yyyy"
    ```

    Note that the format must be `dd/mm/yyyy`and the file has to be a version of `public/data/latest/owid-covid-latest.csv`, in the covid-19 repo.
    To get the latter file, one must:
    1) Go to the covid-19 repo of owid
    2) checkout the last commit of the day of interest
    3) copy file public/data/latest/owid-covid-latest.csv (also rename it to something that makes sense)
    4) run the script described above
5) update_mongo_for_date

    A script for manual insertion of data from Our World In Data. This is for a given date that might had problem (cronjob failure, or failed connection with OWID api etc)
6) rasa_e2e_test

    A folder that has a python script to check story paths with examples in an end to end fashion. This was made due to the inabillity of rasa test to check what happened during the time of an action call. 

    To run those tests
    ```
    cd rasa_e2e_test
    python rasa_e2e_test.py <name of the files to test>
    ```

    This will produce a log with Green colour for the tests that passed, Red for the tests that failed and the fraction of the tests that passed with respect to the total number of tests.

    The naming convention is: story_{testing_action}_{identifier}.txt

---
## External services and their respective place

1) MongoDB

    The database is loaded on host  "dialogue.ilsp.gr" and port 8894
    The environment variables that need to be set are "MONGO_SERVICE_HOST" and "MONGO_SERVICE_PORT".
2) REDIS

    REDIS is loaded on host 'dialogue.ilsp.gr' and port 8891.
    The environment variables that need to be set are "REDIS_SERVICE_HOST" and "REDIS_SERVICE_PORT".
3) Duckling

    Duckling is loaded on host 'dialogue.ilsp.gr' and port 8895
    Duckling is needed to be passed as a component in config.yml where there is a parameter url that needs to be filled as followed:
    ```url: http://dialogue.ilsp.gr:8895```
4) Action server endpoint

    From endpoints.yml, uncomment the section for the action server and change the url for the action server service if it isn't avalaible locally.

---
## Pushing to production

To push a new version of the chatbot into production, we need to login the deployment servers, and connect to the correct tmux sessions:
* For dialogue: `DPLMNT`
* For dialogue3: `RASA`

Start by checking out the commit to be released, normally latest commit of master. Then you either need to train the new model, or use a readymade one. In the first case you will need to re-generate the extended stories for smart suggestion, and then run
```
rasa train
```
In any case, you need to make sure that 
a) the trained model is inside the `models` directory and
b) that it has the right permissions to be run

Next is the step of ensuring that the docker-compose files (`docker-compose.yml` and `docker-compose.override.yml` *in the superproject*) and the docker files are in order. Specifically check:
the environment variables to be passed to the containers at the 'up-time'.

Important vars to check (the last four are used to connect us to RasaX):

- `NO_ACTION_PROBABILITY=0.8` (or 0.2, depeding on AB-Testing, only for rasa_actions)
- `REDIS_MASTER_SERVICE_HOST=redis`
- `REDIS_MASTER_SERVICE_PORT=6379`
- `MONGO_SERVICE_HOST=mongo` 
- `MONGO_SERVICE_PORT=27017`
- `MONGO_DB_NAME=rasa_group_A` (or `rasa_group_B` depending on AB-Testing)
- `RABBITMQ_PASSWORD=some-very-long-ascii-string`
- `RABBITMQ_HOST=dialogue.ilsp.gr`
- `RABBITMQ_USERNAME=user`
- `RABBITMQ_QUEUE=rasa_production_events`


Ensure that duckling, mongo and redis are running and their version is correct:
```
duckling: kozmas/ilsp-duckling:latest
mongo: 4.0
redis: latest
```

Then, make sure mongo is up to date. For that, we have [MongoDB Compass](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwizpta_5fPvAhVEsaQKHe4PBuQQFjABegQIEhAD&url=https%3A%2F%2Fwww.mongodb.com%2Ftry%2Fdownload%2Fcompass&usg=AOvVaw26hcbqmSeE0D9pF0kKi2EO), which works on windows, linux and macOS. (See the mongo endpoint [here](#external-services-and-their-respective-place))  

and that the cron jobs are working correctly, by checking the cron jobs are executing the following scripts:
* `dialogue_update_va.sh`: (Nyrros xls and `covid_api_greek2english_country` collection in mongo)
* `dialogue_update_va_nyrros.sh`: (Just for Nyrros xls) 
* `dialogue_update_owid_repo.sh`: (for the info in owid repo)

check the last update times reported in `dialogue_va.log`:

```
tail dialogue_va.log
```

After all this is done run

```
docker-compose up -d --build rasa rasa_actions 
```

to update the services. This will make them stop for around 30 seconds, so better avoid doing this at peak traffic times. 

### How to test the deployed chatbot

You should test each group separately. For [group A](https://apps.ilsp.gr/covid-va/chatbotA/) visit this page, and [this for group B](https://apps.ilsp.gr/covid-va/chatbotΒ/)

* Ask for data: yesterdays greek cases, yesterdays spanish cases, yesterdays vaccinations, yesterdays icu availability.
* Ensure that group A's Theano suggests topics much less frequently than group B's Theano. 
* Ensure that conversations are stored to [Rasa X conversations](http://dialogue.ilsp.gr/conversations/)
* Ensure that conversations are stored in mongoDB (using Compass and filter `{ latest_event_time : { $gt : 161XXXXXXX}}`, replace value with actual unix timestamp)
* Try the latest functionality to ensure the latest chatbot was deployed
* Run some basic flows to ensure nothing goes wrong with the chatbot
* See if the chatbot stops responding after the final message

---


## RasaX

RasaX is an online GUI that provides various features. In this project we use it to:
1) Save conversations: All the incoming conversations are saved in the section "Conversations", and a member of our team annotates the errors depending on the severity with a 3-scale system (good-some problems-urgent)
2) Add messages from inbox in the NLU training data: Every message appears in the NLU Inbox section, with the predicted intent on the side. A member of our team corrects the wrong predictions and adds the users' messages in the training dataset, by pressing "Mark as correct"

---

## Troubleshooting

Maybe this section has to point to Gitlab issues rather than explaining every detail here. Thus, it should provide a summary of the issues-bugs found and their solution for further reoccurence.

